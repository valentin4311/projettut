import com.gp3.model.ConsoleLogs;
import com.gp3.model.Constants;
import com.gp3.model.GameManager;

public class Launcher
{
	public static void main(String[] args)
	{
		ConsoleLogs.info("Launching " + Constants.GAME_NAME);
		GameManager game = new GameManager();
		game.initGame();
	}
}
