package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import com.gp3.model.RessourcesManager;

public class LoadingPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	private final JProgressBar progressBar = new JProgressBar();

	public LoadingPanel()
	{
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
		progressBar.setIndeterminate(true);
		add(progressBar, BorderLayout.SOUTH);
	}

	public JProgressBar getProgressbar()
	{
		return progressBar;
	}

	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		g.drawImage(RessourcesManager.getPicture("loadingScreen"), 0, 0, getWidth(), getHeight(), null);
	}
}
