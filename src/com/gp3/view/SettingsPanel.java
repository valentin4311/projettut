package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.gp3.controller.ChangeHandler;
import com.gp3.controller.EventHandler;
import com.gp3.model.GameSettings;
import com.gp3.model.Resolution;
import com.gp3.model.RessourcesManager;

public class SettingsPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	private MainMenu parent;
	private JComboBox<String> styleBox;
	private JComboBox<Resolution> resolutionBox;
	private JComboBox<String> musicBox;

	public SettingsPanel(MainMenu menu)
	{
		parent = menu;
		setLayout(new BorderLayout());

		JPanel mainPanel = new JPanel();
		{
			mainPanel.setLayout(new BorderLayout());
			JPanel buttonPanel = new JPanel();
			{
				buttonPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
				buttonPanel.setLayout(new GridLayout(1, 0, 15, 0));

				JButton saveButton = new JButton("Appliquer");
				saveButton.addActionListener(new EventHandler(parent, "saveSettings"));

				JButton loadButton = new JButton("Par defaut");
				loadButton.addActionListener(new EventHandler(parent, "resetSettings"));

				JButton backButton = new JButton("Retour");
				backButton.addActionListener(new EventHandler(parent, "emptyMenu"));

				buttonPanel.add(saveButton);
				buttonPanel.add(loadButton);
				buttonPanel.add(backButton);
			}
			JPanel settingsPanel = new JPanel();
			{
				settingsPanel.setLayout(new BorderLayout());
				settingsPanel.setPreferredSize(new Dimension(300, 400));

				JPanel soundPanel = new JPanel();
				{
					soundPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
					soundPanel.setLayout(new BorderLayout());
					soundPanel.setPreferredSize(new Dimension(300, 100));

					JPanel soundTitle = new JPanel();
					{
						soundTitle.setBorder(new EtchedBorder(EtchedBorder.LOWERED));

						JLabel soundLabel = new JLabel("Son");
						{
							soundLabel.setFont(RenderHelper.COMIC_SANS_MS_18);
						}

						soundTitle.add(soundLabel);
					}
					JPanel soundSettingsPanel = new JPanel();
					{
						soundSettingsPanel.setBorder(new EmptyBorder(6, 6, 6, 6));
						soundSettingsPanel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));

						JLabel sVolumeAmount = new JLabel("Volume des Effets sonores :");

						JSlider soundSlider = new JSlider();
						soundSlider.setValue(GameSettings.soundVolume);
						soundSlider.addChangeListener(new ChangeHandler(parent, "soundVolume", soundSlider));

						JLabel mVolumeAmount = new JLabel("Volume de la Musique :");

						JSlider musicSlider = new JSlider();
						musicSlider.setValue(GameSettings.musicVolume);
						musicSlider.addChangeListener(new ChangeHandler(parent, "musicVolume", musicSlider));

						JLabel Musique = new JLabel("Musique de fond :");

						musicBox = new JComboBox<String>();
						musicBox.setModel(new DefaultComboBoxModel<String>(RessourcesManager.getAllMusic()));
						musicBox.setSelectedItem(GameSettings.selectedMusic);
						musicBox.addActionListener(new EventHandler(parent, "changeMusic"));

						JButton refreshButton = new JButton(new ImageIcon(RessourcesManager.getPicture("refresh")));
						refreshButton.addActionListener(new EventHandler(parent, "refreshRessources"));

						JLabel hintLabel = new JLabel("Ajoutez des musiques (format wav) :");
						JButton openDir = new JButton("ICI");
						openDir.addActionListener(new EventHandler(parent, "openMusicDir"));

						JPanel copyrightPanel = new JPanel();
						{
							copyrightPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));

							JTextPane copyrightArea = new JTextPane();
							copyrightArea.setBackground(copyrightPanel.getBackground());
							copyrightArea.setText("Credits Musiques :\r\nKevin MacLeod (incompetech.com) \r\nLicensed under Creative Commons:\r\nBy Attribution 3.0\r\nhttp://creativecommons.org/licenses/by/3.0/");
							copyrightArea.setEditable(false);

							copyrightPanel.add(copyrightArea);
						}

						soundSettingsPanel.add(sVolumeAmount);
						soundSettingsPanel.add(soundSlider);
						soundSettingsPanel.add(mVolumeAmount);
						soundSettingsPanel.add(musicSlider);
						soundSettingsPanel.add(Musique);
						soundSettingsPanel.add(musicBox);
						soundSettingsPanel.add(refreshButton);
						soundSettingsPanel.add(hintLabel);
						soundSettingsPanel.add(openDir);
						soundSettingsPanel.add(copyrightPanel);
					}
					soundPanel.add(soundTitle, BorderLayout.NORTH);
					soundPanel.add(soundSettingsPanel, BorderLayout.CENTER);

				}
				JPanel leftPanel = new JPanel();
				{
					leftPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
					leftPanel.setLayout(new BorderLayout());

					JPanel displayPanel = new JPanel();
					{
						displayPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));

						JLabel displayLabel = new JLabel("Affichage");
						displayLabel.setFont(RenderHelper.COMIC_SANS_MS_18);
						displayPanel.add(displayLabel);
					}
					JPanel leftResizePanel = new JPanel();
					{
						JPanel fullscreenPanel = new JPanel();
						fullscreenPanel.setLayout(new BorderLayout());

						JPanel stylePanel = new JPanel();
						{
							stylePanel.setBorder(new EmptyBorder(4, 4, 4, 4));
							stylePanel.setLayout(new BorderLayout());

							JLabel styleLabel = new JLabel("Style :");
							stylePanel.add(styleLabel, BorderLayout.NORTH);
						}

						styleBox = new JComboBox<String>();
						styleBox.setModel(new DefaultComboBoxModel<String>(new String[] { "Theme OS", "Metal", "Nimbus", "Business", "Business Acier Bleu", "Business Acier Noir", "Creme", "Creme Cafe", "Sahara", "Modere", "Nebula", "Nebula Brick Wall", "Automne", "Brume Argente", "Brume Aquatique", "Poussiere", "Poussiere cafe", "Gemini", "Satellite", "Crepuscule", "Magellan", "Graphite", "Graphite Vitre", "Graphite Aquatique", "Corbeau", "Challenger Deep", "Poussiere Emeraude" }));
						styleBox.setSelectedItem(GameSettings.gameStyle);
						styleBox.addActionListener(new EventHandler(parent, "changeStyle"));
						stylePanel.add(styleBox, BorderLayout.SOUTH);

						JPanel resolutionPane = new JPanel();
						{
							resolutionPane.setBorder(new EmptyBorder(4, 4, 4, 4));
							resolutionPane.setLayout(new BorderLayout());

							JLabel resolutionLabel = new JLabel("Resolution de l'Ecran :");

							resolutionBox = new JComboBox<Resolution>();
							resolutionBox.setEnabled(GameSettings.isFullScreen);
							ArrayList<Resolution> list = GameSettings.getAllAvailableResolution();
							Resolution[] modes = new Resolution[list.size()];
							list.toArray(modes);
							resolutionBox.setModel(new DefaultComboBoxModel<Resolution>(modes));
							resolutionBox.setSelectedIndex(GameSettings.getIndexForSize(GameSettings.currentWidth, GameSettings.currentHeight));
							resolutionBox.addActionListener(new EventHandler(parent, "changeResolution"));

							resolutionPane.add(resolutionBox, BorderLayout.SOUTH);
							resolutionPane.add(resolutionLabel, BorderLayout.NORTH);
						}

						JCheckBox fullScreenBox = new JCheckBox("Plein Ecran");
						fullScreenBox.setSelected(GameSettings.isFullScreen);
						fullScreenBox.addActionListener(new EventHandler(parent, "toggleFullscreen"));
						GroupLayout gl_leftResizePanel = new GroupLayout(leftResizePanel);
						gl_leftResizePanel.setHorizontalGroup(gl_leftResizePanel.createParallelGroup(Alignment.LEADING).addGroup(Alignment.TRAILING, gl_leftResizePanel.createSequentialGroup().addGap(2).addGroup(gl_leftResizePanel.createParallelGroup(Alignment.TRAILING).addComponent(resolutionPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE).addComponent(stylePanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE).addGroup(Alignment.LEADING, gl_leftResizePanel.createSequentialGroup().addComponent(fullScreenBox).addPreferredGap(ComponentPlacement.UNRELATED).addComponent(fullscreenPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))).addContainerGap()));
						gl_leftResizePanel.setVerticalGroup(gl_leftResizePanel.createParallelGroup(Alignment.LEADING).addGroup(gl_leftResizePanel.createSequentialGroup().addGroup(gl_leftResizePanel.createParallelGroup(Alignment.LEADING).addComponent(fullscreenPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addGroup(gl_leftResizePanel.createSequentialGroup().addContainerGap().addComponent(fullScreenBox))).addPreferredGap(ComponentPlacement.RELATED).addComponent(stylePanel, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE).addGap(2).addComponent(resolutionPane, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE).addGap(68)));
						leftResizePanel.setLayout(gl_leftResizePanel);
					}
					leftPanel.add(leftResizePanel);
					leftPanel.add(displayPanel, BorderLayout.NORTH);
				}
				settingsPanel.add(leftPanel, BorderLayout.CENTER);
				settingsPanel.add(soundPanel, BorderLayout.EAST);
			}
			mainPanel.add(settingsPanel, BorderLayout.CENTER);
			mainPanel.add(buttonPanel, BorderLayout.SOUTH);
		}
		JLabel settingsLabel = new JLabel("Options");
		settingsLabel.setHorizontalAlignment(SwingConstants.CENTER);
		settingsLabel.setFont(RenderHelper.COMIC_SANS_MS_18);

		add(mainPanel, BorderLayout.CENTER);
		add(settingsLabel, BorderLayout.NORTH);
	}

	public String getStyleBoxValue()
	{
		return (String) styleBox.getSelectedItem();
	}

	public String getMusicBoxValue()
	{
		return (String) musicBox.getSelectedItem();
	}

	public void refreshMusicBox()
	{
		musicBox.setModel(new DefaultComboBoxModel<String>(RessourcesManager.getAllMusic()));
		musicBox.repaint();
	}

	public Resolution getResolutionBoxValue()
	{
		return (Resolution) resolutionBox.getSelectedItem();
	}

	public JComboBox<Resolution> getResolutionBox()
	{
		return resolutionBox;
	}
}
