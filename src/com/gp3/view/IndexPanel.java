package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.File;

import javax.swing.JEditorPane;
import javax.swing.JPanel;

public class IndexPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	public IndexPanel()
	{
		setLayout(new BorderLayout());
		JEditorPane html = new JEditorPane();
		html.setBackground(Color.DARK_GRAY);
		html.setEditable(false);
		try
		{
			File file = new File("./assets/index.html");
			html.setPage(file.toURI().toURL());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			html.setText("Impossible de trouver le fichier ./assets/index.html");
		}
		add(html, BorderLayout.CENTER);
	}

}
