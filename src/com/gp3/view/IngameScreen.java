package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;

import com.gp3.controller.EventHandler;
import com.gp3.controller.MouseHandler;
import com.gp3.model.GameManager;
import com.gp3.model.TimerThread;

public class IngameScreen extends JPanel implements GUI
{
	private static final long serialVersionUID = 1L;

	private PauseMenu pauseMenu;
	private static JLabel questionAmountLabel;
	private static JLabel questLabel;
	private static JLabel scoreLabel;

	public static void setQuestionLabel(int currentQuestion, int maxQuestion)
	{
		questionAmountLabel.setText((currentQuestion) + " / " + maxQuestion + "  ");
	}

	public static void setQuestLabel(String location)
	{
		questLabel.setText("Situez " + location + " !");
	}

	public static void updateScoreLabel()
	{
		scoreLabel.setText(String.valueOf(GameManager.getInstance().getCurrentGame().getScore()));
	}

	@Override
	public void initGUI()
	{
		setLayout(new BorderLayout());

		JPanel topPanel = new JPanel();
		{
			topPanel.setBorder(new EtchedBorder(EtchedBorder.RAISED));
			topPanel.setLayout(new BorderLayout());

			JButton menuButton = new JButton("Menu");
			menuButton.setPreferredSize(new Dimension(80, 28));
			menuButton.setFont(RenderHelper.COMIC_SANS_MS_14);
			menuButton.addActionListener(new EventHandler(this, "pauseMenu"));

			questLabel = new JLabel("Situez");
			questLabel.setHorizontalAlignment(SwingConstants.CENTER);
			questLabel.setFont(RenderHelper.COMIC_SANS_MS_18);

			questionAmountLabel = new JLabel("0 / 0 ");
			questionAmountLabel.setHorizontalAlignment(SwingConstants.TRAILING);
			questionAmountLabel.setPreferredSize(new Dimension(80, 28));
			questionAmountLabel.setFont(RenderHelper.COMIC_SANS_MS_14);

			topPanel.add(questLabel, BorderLayout.CENTER);
			topPanel.add(menuButton, BorderLayout.WEST);
			topPanel.add(questionAmountLabel, BorderLayout.EAST);
		}

		BottomPanel bottomPanel = new BottomPanel();
		{
			bottomPanel.setBorder(new EtchedBorder(EtchedBorder.RAISED));
			bottomPanel.setPreferredSize(new Dimension(80, 28));
			bottomPanel.setLayout(new BorderLayout());

			scoreLabel = new JLabel("42");
			scoreLabel.setHorizontalAlignment(SwingConstants.CENTER);
			scoreLabel.setFont(RenderHelper.COMIC_SANS_MS_18);
			bottomPanel.add(scoreLabel, BorderLayout.CENTER);
		}

		MapPanel mainPanel = new MapPanel();
		mainPanel.addMouseListener(new MouseHandler());

		pauseMenu = new PauseMenu(this);

		add(topPanel, BorderLayout.NORTH);
		add(mainPanel, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.SOUTH);
		add(pauseMenu, BorderLayout.WEST);
	}

	@Override
	public void refreshGUI()
	{
		repaint();
	}

	@Override
	public void handleAction(ActionEvent event, String buttonID)
	{
		if (buttonID.equals("pauseMenu"))
		{
			pauseMenu.setVisible(!PauseMenu.isOpen());
		}
		else if (buttonID.equals("closeMenu"))
		{
			pauseMenu.dispose();
		}
		else if (buttonID.equals("saveGame"))
		{
			GameManager.getInstance().getCurrentGame().saveGame();
		}
		else if (buttonID.equals("surrender"))
		{
			int result = JOptionPane.showInternalConfirmDialog(this, "Voulez-vous vraiment abandonner la partie ?", "Abandonner la partie ?", JOptionPane.YES_NO_OPTION);

			if (result == JOptionPane.YES_OPTION)
			{
				TimerThread.endTimer();
				pauseMenu.dispose();
				GameManager.getInstance().displayGUI(new MainMenu());
				GameManager.getInstance().setCurrentGame(null);
			}
		}
		else if (buttonID.equals("quitGame"))
		{
			int result = JOptionPane.showInternalConfirmDialog(this, "Voulez-vous vraiment quitter le jeu ?", "Quitter le jeu ?", JOptionPane.YES_NO_OPTION);

			if (result == JOptionPane.YES_OPTION)
			{
				System.exit(0);
			}
		}
	}

	@Override
	public void handleChangeEvent(ChangeEvent event, String buttonID, JSlider slider)
	{}

	@Override
	public void destroyGUI()
	{}

	@Override
	public JPanel getPanel()
	{
		return this;
	}

}
