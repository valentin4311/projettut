package com.gp3.view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import com.gp3.model.EnumGameMode;
import com.gp3.model.Game;
import com.gp3.model.GameManager;
import com.gp3.model.RessourcesManager;

public class BottomPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		Game game = GameManager.getInstance().getCurrentGame();
		EnumGameMode mode = game.getGamemode();

		if (mode == EnumGameMode.ARCADE)
		{
			int percent = (int) ((double) ((double) game.getTimeLeft() / game.getMaxTime()) * 200);
			float colorValue = (float) game.getTimeLeft() / (float) game.getMaxTime() / 4F;
			Color barColor = Color.getHSBColor(colorValue, 1.0F, 1.0F);
			g.setColor(barColor);
			g.fillRect(getWidth() - 212, 3, percent, 27);
			g.drawImage(RessourcesManager.getPicture("timeBar"), getWidth() - 212, 3, null);

			percent = (int) ((double) ((double) game.getTotalTime() / game.getMaxTotalTime()) * 200);
			colorValue = (float) game.getTotalTime() / (float) game.getMaxTotalTime() / 4F;
			barColor = Color.getHSBColor(colorValue, 1.0F, 1.0F);
			g.setColor(barColor);
			g.fillRect(212 - percent, 3, percent, 27);
			g.drawImage(RessourcesManager.getPicture("timeBar2"), -12, 3, null);
		}
		else if (mode == EnumGameMode.SURVIVAL || mode == EnumGameMode.ADVENTURE)
		{
			for (int i = 0; i < game.getLifeAmount(); i++)
			{
				g.drawImage(RessourcesManager.getPicture("heart"), 5 + 18 * i, 6, null);
			}
			int percent = (int) ((double) ((double) game.getTimeLeft() / game.getMaxTime()) * 200);
			float colorValue = (float) game.getTimeLeft() / (float) game.getMaxTime() / 4F;
			Color barColor = Color.getHSBColor(colorValue, 1.0F, 1.0F);
			g.setColor(barColor);
			g.fillRect(getWidth() - 212, 3, percent, 27);
			g.drawImage(RessourcesManager.getPicture("timeBar"), getWidth() - 212, 3, null);
		}
	}
}
