package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.gp3.controller.EventHandler;
import com.gp3.model.EnumGameMode;
import com.gp3.model.GameManager;
import com.gp3.model.RessourcesManager;

public class PauseMenu extends JInternalFrame
{
	private static final long serialVersionUID = 1L;

	private static boolean isOpen = false;
	private static JButton saveButton;

	private GUI parent;

	public PauseMenu(GUI ingameScreen)
	{
		super("Menu Pause");
		parent = ingameScreen;
		setFrameIcon(new ImageIcon(RessourcesManager.getPicture("favicon_128").getScaledInstance(16, 16, 16)));
		setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		setSize(200, 300);
		setResizable(false);
		getContentPane().setLayout(new BorderLayout());

		JLabel pauseLabel = new JLabel("Pause");
		pauseLabel.setHorizontalAlignment(SwingConstants.CENTER);
		pauseLabel.setFont(RenderHelper.COMIC_SANS_MS_22);

		JPanel buttonPanel = new JPanel();
		{
			buttonPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
			buttonPanel.setLayout(new GridLayout(4, 1, 0, 15));

			JButton continueButton = new JButton(GameManager.getInstance().getCurrentGame().getGamemode() == EnumGameMode.EDITOR ? "Reprendre l'�dition" : "Reprendre la partie");
			continueButton.addActionListener(new EventHandler(parent, "closeMenu"));
			continueButton.setFont(RenderHelper.COMIC_SANS_MS_14);

			saveButton = new JButton(GameManager.getInstance().getCurrentGame().getGamemode() == EnumGameMode.EDITOR ? "Sauvegarder la carte" : "Sauvegarder la partie");
			saveButton.addActionListener(new EventHandler(parent, "saveGame"));
			saveButton.setFont(RenderHelper.COMIC_SANS_MS_14);
			saveButton.setEnabled(GameManager.getInstance().getCurrentGame().getGamemode() != EnumGameMode.ZEN && !(ingameScreen instanceof DialogPanel));
			PauseMenu.updateButton();

			JButton surrenderButton = new JButton(GameManager.getInstance().getCurrentGame().getGamemode() == EnumGameMode.EDITOR ? "Retourner � l'�cran titre" : "Abandonner la partie");
			surrenderButton.addActionListener(new EventHandler(parent, "surrender"));
			surrenderButton.setFont(RenderHelper.COMIC_SANS_MS_14);

			JButton quitButton = new JButton("Quitter le jeu");
			quitButton.addActionListener(new EventHandler(parent, "quitGame"));
			quitButton.setFont(RenderHelper.COMIC_SANS_MS_14);

			buttonPanel.add(continueButton);
			buttonPanel.add(saveButton);
			buttonPanel.add(surrenderButton);
			buttonPanel.add(quitButton);
		}
		getContentPane().add(pauseLabel, BorderLayout.NORTH);
		getContentPane().add(buttonPanel, BorderLayout.CENTER);
		setMouseCursor();
		pack();
	}

	public static void updateButton()
	{
		if (GameManager.getInstance().getCurrentGame().getGamemode() == EnumGameMode.EDITOR)
		{
			saveButton.setEnabled(EditorScreen.isMapCompleted());
			saveButton.setText(EditorScreen.isMapCompleted() ? "Sauvegarder la carte" : "Sauvegarde : Carte incompl�te !");
		}
	}

	public static boolean isOpen()
	{
		return isOpen;
	}

	public static void setOpen(boolean isOpen)
	{
		PauseMenu.isOpen = isOpen;
	}

	public void setMouseCursor()
	{
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image image = RessourcesManager.getPicture("cursor");
		Cursor c = toolkit.createCustomCursor(image, new Point(0, 0), "img");
		setCursor(c);
	}

	@Override
	public void setVisible(boolean v)
	{
		super.setVisible(v);
		isOpen = v;
	}

	@Override
	public void dispose()
	{
		super.dispose();
		isOpen = false;
	}
}
