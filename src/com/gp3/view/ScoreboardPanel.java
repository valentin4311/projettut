package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.gp3.controller.EventHandler;
import com.gp3.model.Score;
import com.gp3.model.ScoresManager;

public class ScoreboardPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	private MainMenu parent;
	private JTable table;

	private JComboBox<String> comboBox;
	
	public ScoreboardPanel(MainMenu menu)
	{
		parent = menu;
		setLayout(new BorderLayout(0, 0));

		JLabel scoreboardLabel = new JLabel("Tableau des scores");
		scoreboardLabel.setHorizontalAlignment(SwingConstants.CENTER);
		scoreboardLabel.setFont(RenderHelper.COMIC_SANS_MS_18);

		JPanel mainPanel = new JPanel();
		{
			mainPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
			mainPanel.setLayout(new BorderLayout(0, 0));

			JPanel gamemodePanel = new JPanel();
			{
				gamemodePanel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));

				JLabel gamemodeLabel = new JLabel("Mode de jeu :");

				comboBox = new JComboBox<String>();
				comboBox.setModel(new DefaultComboBoxModel<String>(new String[] { "Aventure", "Arcade", "Survie" }));
				comboBox.addActionListener(new EventHandler(parent, "changeScoreboard"));

				gamemodePanel.add(gamemodeLabel);
				gamemodePanel.add(comboBox);
			}

			table = new JTable(new NonEditableTable(null, new String[] { "Nom", "Score", "Difficulté" }));
			table.setFillsViewportHeight(true);
			updateTable(0);
			JScrollPane scrollPane = new JScrollPane(table);

			mainPanel.add(gamemodePanel, BorderLayout.NORTH);
			mainPanel.add(scrollPane, BorderLayout.CENTER);
		}

		JPanel buttonPanel = new JPanel();
		{
			buttonPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
			buttonPanel.setLayout(new GridLayout(1, 0, 15, 0));

			JButton resetScoresButton = new JButton("Effacer les scores");
			resetScoresButton.addActionListener(new EventHandler(parent, "clearScores"));

			JButton backButton = new JButton("Retour");
			backButton.addActionListener(new EventHandler(parent, "emptyMenu"));

			buttonPanel.add(resetScoresButton);
			buttonPanel.add(backButton);
		}

		add(scoreboardLabel, BorderLayout.NORTH);
		add(mainPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	public void updateTable(int id)
	{
		ArrayList<Score> scoreList = ScoresManager.getInstance().getScoreList(id);
		
		int lines = ((NonEditableTable)table.getModel()).getRowCount();
		for (int i = lines - 1; i >= 0; i--)
		{
			((NonEditableTable)table.getModel()).removeRow(i);
		}
		
		for(Score sc : scoreList)
		{
			((NonEditableTable)table.getModel()).addRow(new Object[]{sc.getPlayerName(), sc.getScore(), sc.getDifficulty()});
		}
	}

	public JComboBox<String> getComboBox()
	{
		return comboBox;
	}
}
