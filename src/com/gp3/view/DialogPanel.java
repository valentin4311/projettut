package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;

import com.gp3.controller.EventHandler;
import com.gp3.model.Game;
import com.gp3.model.GameManager;
import com.gp3.model.SlowMessageThread;

public class DialogPanel extends JPanel implements GUI
{
	private static final long serialVersionUID = 1L;
	
	private JTextPane textPane;
	private JButton nextButton;

	private String[] messages;
	private static SlowMessageThread thread;
	private int currentMessage = 0;
	
	private PauseMenu pauseMenu;
	private int actionID = 0;
	
	
	public DialogPanel(String[] messages, int actionID)
	{
		this.setMessages(messages);
		this.actionID = actionID;
		thread = new SlowMessageThread();
		thread.start();
	}

	@Override
	public void initGUI()
	{
		setLayout(new BorderLayout());
		
		JPanel topBar = new JPanel();
		topBar.setLayout(new BorderLayout());
		topBar.setBorder(new EtchedBorder(EtchedBorder.RAISED));
		
		JButton menuButton = new JButton(" Menu ");
		menuButton.addActionListener(new EventHandler(this, "pauseMenu"));
		
		JLabel titleLabel = new JLabel("Mode aventure");
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		titleLabel.setFont(RenderHelper.COMIC_SANS_MS_18);
		
		JButton skipButton = new JButton("Passer");
		skipButton.addActionListener(new EventHandler(this, "skip"));
		
		topBar.add(menuButton, BorderLayout.WEST);
		topBar.add(titleLabel, BorderLayout.CENTER);
		topBar.add(skipButton, BorderLayout.EAST);
		
		JPanel dialogPane = new JPanel();
		dialogPane.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
		dialogPane.setLayout(new BorderLayout());
		
		textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setFont(RenderHelper.COMIC_SANS_MS_14);
		
		nextButton = new JButton("Suite");
		nextButton.setEnabled(false);
		nextButton.addActionListener(new EventHandler(this, "next"));
		
		dialogPane.add(nextButton, BorderLayout.EAST);
		dialogPane.add(textPane, BorderLayout.CENTER);
		
		pauseMenu = new PauseMenu(this);
		
		add(pauseMenu, BorderLayout.WEST);
		add(topBar, BorderLayout.NORTH);
		add(dialogPane, BorderLayout.SOUTH);
	}

	@Override
	public void refreshGUI()
	{
		if(textPane != null && !PauseMenu.isOpen())
		{
			messages[currentMessage] = messages[currentMessage].replace("%n", GameManager.getInstance().getCurrentGame().getStory().getHeroName());
			int currentLenght = textPane.getText().length();
			
			if(currentLenght < messages[currentMessage].length())
			{
				textPane.setText(messages[currentMessage].substring(0, ++currentLenght));
				textPane.repaint();
			}
			else
			{
				nextButton.setEnabled(true);
			}
		}
	}

	@Override
	public void handleAction(ActionEvent event, String buttonID)
	{
		if(buttonID.equals("next") && !PauseMenu.isOpen())
		{
			if(currentMessage < messages.length - 1)
			{
				nextButton.setEnabled(false);
				currentMessage++;
				textPane.setText("");
			}
			else
			{
				skip();
			}
		}
		else if(buttonID.equals("skip") && !PauseMenu.isOpen())
		{
			skip();
		}
		else if (buttonID.equals("pauseMenu"))
		{
			pauseMenu.setVisible(!PauseMenu.isOpen());
		}
		else if (buttonID.equals("closeMenu"))
		{
			pauseMenu.dispose();
		}
		else if (buttonID.equals("surrender"))
		{
			int result = JOptionPane.showInternalConfirmDialog(this, "Voulez-vous vraiment retourner � l'�cran titre ?", "Retourner � l'�cran titre ?", JOptionPane.YES_NO_OPTION);

			if (result == JOptionPane.YES_OPTION)
			{
				pauseMenu.dispose();
				thread.stopThread();
				GameManager.getInstance().displayGUI(new MainMenu());
				GameManager.getInstance().setCurrentGame(null);
			}
		}
		else if (buttonID.equals("quitGame"))
		{
			int result = JOptionPane.showInternalConfirmDialog(this, "Voulez-vous vraiment quitter le jeu ?", "Quitter le jeu ?", JOptionPane.YES_NO_OPTION);

			if (result == JOptionPane.YES_OPTION)
			{
				System.exit(0);
			}
		}
	}

	public void skip()
	{
		thread.stopThread();
		
		if(actionID == 0)
		{
			GameManager.getInstance().displayGUI(new IngameScreen());
			GameManager.getInstance().getCurrentGame().initStoryGame();
		}
		else if(actionID == 1)
		{
			GameManager.getInstance().getCurrentGame().winAdventure();
		}
		else
		{
			GameManager.getInstance().getCurrentGame().looseGame();
		}
	}
	
	@Override
	public void handleChangeEvent(ChangeEvent event, String buttonID, JSlider slider)
	{

	}

	@Override
	public void destroyGUI()
	{

	}

	@Override
	public JPanel getPanel()
	{
		return this;
	}

	public String[] getMessages()
	{
		return messages;
	}

	public void setMessages(String[] messages)
	{
		this.messages = messages;
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Game game = GameManager.getInstance().getCurrentGame();
		g.drawImage(game.getCurrentMap().getMapImage(), 0, 0, getWidth(), getHeight(), null);
	}
	
}
