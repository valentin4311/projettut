package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.gp3.controller.EventHandler;
import com.gp3.model.EnumDifficulty;
import com.gp3.model.EnumGameMode;
import com.gp3.model.SaveManager;

public class LoadPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	private MainMenu parent;
	private JTable table;

	private final String[] MONTHS = {"Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre"};
	
	public LoadPanel(MainMenu mainMenu)
	{
		parent = mainMenu;
		setLayout(new BorderLayout());

		JLabel loadLabel = new JLabel("Reprendre une Partie");
		loadLabel.setFont(RenderHelper.COMIC_SANS_MS_18);
		loadLabel.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel mainPanel = new JPanel();
		{
			mainPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
			mainPanel.setLayout(new BorderLayout());

			table = new JTable(new NonEditableTable(null, new String[] { "Date", "Mode de jeu", "Difficult�" }));
			table.setFillsViewportHeight(true);
			updateTable();
			
			JScrollPane scrollPane = new JScrollPane(table);

			mainPanel.add(scrollPane);
		}

		JPanel buttonPanel = new JPanel();
		{
			buttonPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
			buttonPanel.setLayout(new GridLayout(1, 0, 15, 0));

			JButton loadButton = new JButton("Reprendre la partie");
			loadButton.addActionListener(new EventHandler(parent, "loadSave"));

			JButton deleteButton = new JButton("Effacer la partie");
			deleteButton.addActionListener(new EventHandler(parent, "deleteSave"));

			JButton backButton = new JButton("Retour");
			backButton.addActionListener(new EventHandler(parent, "playMenu"));

			buttonPanel.add(loadButton);
			buttonPanel.add(deleteButton);
			buttonPanel.add(backButton);
		}

		add(loadLabel, BorderLayout.NORTH);
		add(mainPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}
	
	public void updateTable()
	{
		ArrayList<String> saveList = SaveManager.getAllSaves();
		
		int lines = ((NonEditableTable)table.getModel()).getRowCount();
		for (int i = lines - 1; i >= 0; i--)
		{
			((NonEditableTable)table.getModel()).removeRow(i);
		}
		
		for(String sg : saveList)
		{
			String[] parts = sg.substring(0, sg.length() - 4).split("-");
			((NonEditableTable)table.getModel()).addRow(new Object[]{"Le " + parts[0] + " " + MONTHS[Integer.valueOf(parts[1])] + " " + parts[2] + " � " + parts[3] + "H" + parts[4], EnumGameMode.values()[Integer.valueOf(parts[6])], EnumDifficulty.values()[Integer.valueOf(parts[7])]});
		}
	}
	
	public int getSelectedIndex()
	{
		return table.getSelectedRow();
	}
}
