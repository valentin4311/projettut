package com.gp3.view;

import java.awt.Graphics;

import javax.swing.JPanel;

import com.gp3.model.Map;
import com.gp3.model.RessourcesManager;

public class MapOverviewPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	private Map overviewMap = RessourcesManager.getMap("test");

	public Map getOverviewMap()
	{
		return overviewMap;
	}

	public void setOverviewMap(Map overviewMap)
	{
		this.overviewMap = overviewMap;
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(overviewMap.getMapImage(), 0, 0, getWidth(), getHeight(), null);
	}
}
