package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.gp3.controller.EventHandler;
import com.gp3.model.EnumDifficulty;
import com.gp3.model.EnumGameMode;

import javax.swing.border.EtchedBorder;

public class PlayPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	private MainMenu parent;
	private JTabbedPane gamemodeTab;
	private JTabbedPane difficultyTab;

	public PlayPanel(MainMenu menu)
	{
		parent = menu;
		setLayout(new BorderLayout());

		JLabel playLabel = new JLabel("Jouer");
		playLabel.setHorizontalAlignment(SwingConstants.CENTER);
		playLabel.setFont(RenderHelper.COMIC_SANS_MS_18);

		JPanel mainPanel = new JPanel();
		{
			mainPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
			mainPanel.setLayout(new BorderLayout());

			JPanel gamemodePanel = new JPanel();
			{
				gamemodePanel.setPreferredSize(new Dimension(100, 120));
				gamemodePanel.setLayout(new BoxLayout(gamemodePanel, BoxLayout.Y_AXIS));

				JLabel gamemodeLabel = new JLabel("  Mode de jeu :");
				gamemodeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
				gamemodeLabel.setVerticalAlignment(SwingConstants.BOTTOM);
				gamemodeLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 14));
				gamemodeLabel.setHorizontalAlignment(SwingConstants.LEFT);

				gamemodeTab = new JTabbedPane(JTabbedPane.TOP);
				gamemodeTab.setPreferredSize(new Dimension(100, 200));

				JPanel adventureTab = new JPanel();
				{
					adventureTab.setLayout(new BorderLayout());

					JLabel adventureDesc = new JLabel("<html> Vivez une incroyable aventure !<br> - Vous avez des vies<br> - Vous avez du temps</html>");
					adventureDesc.setVerticalAlignment(SwingConstants.TOP);
					adventureTab.add(adventureDesc, BorderLayout.CENTER);
				}

				JPanel arcadeTab = new JPanel();
				{
					arcadeTab.setLayout(new BorderLayout());

					JLabel arcadeDesc = new JLabel("<html> Le temps s'�coule, r�pondez vite, r�pondez juste.<br> - Vous avez un temps g�n�rale<br> - Vous avez un temps par question</html>");
					arcadeDesc.setVerticalAlignment(SwingConstants.TOP);
					arcadeTab.add(arcadeDesc);
				}

				JPanel survivalTab = new JPanel();
				{
					survivalTab.setLayout(new BorderLayout());

					JLabel survivalDesc = new JLabel("<html> Vous devez r�pondre juste, sinon perdez une vie.<br> - Vous avez des vies</html>");
					survivalDesc.setVerticalAlignment(SwingConstants.TOP);
					survivalTab.add(survivalDesc, BorderLayout.CENTER);
				}
				JPanel zenTab = new JPanel();
				{
					zenTab.setLayout(new BorderLayout());

					JLabel zenDesc = new JLabel("<html> Entra�nez-vous.<br> - Absence de temps et de vie");
					zenDesc.setVerticalAlignment(SwingConstants.TOP);
					zenTab.add(zenDesc, BorderLayout.NORTH);
				}

				gamemodeTab.addTab("Aventure", adventureTab);
				gamemodeTab.addTab("Arcade", arcadeTab);
				gamemodeTab.addTab("Survie", survivalTab);
				gamemodeTab.addTab("Zen", zenTab);
				gamemodeTab.setSelectedIndex(0);
				gamemodePanel.add(gamemodeLabel);
				gamemodePanel.add(gamemodeTab);
			}

			JPanel difficultyPanel = new JPanel();
			{
				difficultyPanel.setLayout(new BoxLayout(difficultyPanel, BoxLayout.Y_AXIS));

				JLabel difficultyLabel = new JLabel("Difficult� :");
				difficultyLabel.setVerticalAlignment(SwingConstants.BOTTOM);
				difficultyLabel.setFont(RenderHelper.COMIC_SANS_MS_14);
				difficultyLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

				difficultyTab = new JTabbedPane(JTabbedPane.TOP);

				JPanel easyTab = new JPanel();
				{
					easyTab.setLayout(new BorderLayout());

					JLabel easyLabel = new JLabel("<html> - Vous avez beaucoup de temps<br> - Vous avez beaucoup de vie</html>");
					easyLabel.setVerticalAlignment(SwingConstants.TOP);
					easyTab.add(easyLabel, BorderLayout.CENTER);
				}

				JPanel mediumTab = new JPanel();
				{
					mediumTab.setLayout(new BorderLayout());

					JLabel mediumLabel = new JLabel("<html> - Le temps s'�coule plus vite<br> - Vous avez moins de vie</html>");
					mediumLabel.setVerticalAlignment(SwingConstants.TOP);
					mediumTab.add(mediumLabel);
				}

				JPanel hardTab = new JPanel();
				{
					hardTab.setLayout(new BorderLayout());

					JLabel hardLabel = new JLabel("<html> - Le temps s'�coule encore plus vite<br> - Vous avez encore moins de vie</html>");
					hardLabel.setVerticalAlignment(SwingConstants.TOP);
					hardTab.add(hardLabel);
				}

				JPanel impossibleTab = new JPanel();
				{
					impossibleTab.setLayout(new BorderLayout());

					JLabel impossibleLabel = new JLabel("<html> - Le temps s'�coule beaucoup plus vite<br> - Vous n'avez qu'une vie</html>");
					impossibleLabel.setVerticalAlignment(SwingConstants.TOP);
					impossibleTab.add(impossibleLabel);
				}
				difficultyTab.addTab("Facile", easyTab);
				difficultyTab.addTab("Moyen", mediumTab);
				difficultyTab.addTab("Difficile", hardTab);
				difficultyTab.addTab("Impossible", impossibleTab);

				difficultyPanel.add(difficultyLabel);
				difficultyPanel.add(difficultyTab);
			}
			mainPanel.add(gamemodePanel, BorderLayout.NORTH);
			mainPanel.add(difficultyPanel, BorderLayout.CENTER);
		}

		JPanel buttonPanel = new JPanel();
		{
			buttonPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
			buttonPanel.setLayout(new GridLayout(1, 0, 15, 0));

			JButton playButton = new JButton("Jouer");
			playButton.addActionListener(new EventHandler(parent, "startNewGame"));

			JButton loadButton = new JButton("Reprendre partie");
			loadButton.addActionListener(new EventHandler(parent, "loadMenu"));

			JButton backButton = new JButton("Retour");
			backButton.addActionListener(new EventHandler(parent, "emptyMenu"));

			buttonPanel.add(playButton);
			buttonPanel.add(loadButton);
			buttonPanel.add(backButton);
		}

		add(playLabel, BorderLayout.NORTH);
		add(mainPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	public EnumGameMode getSelectedGameMode()
	{
		return EnumGameMode.values()[gamemodeTab.getSelectedIndex()];
	}

	public EnumDifficulty getSelectedDifficulty()
	{
		return EnumDifficulty.values()[difficultyTab.getSelectedIndex()];
	}
}
