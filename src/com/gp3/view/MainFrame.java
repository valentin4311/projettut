package com.gp3.view;

import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import javax.swing.JFrame;

import com.gp3.model.Constants;
import com.gp3.model.GameManager;
import com.gp3.model.GameSettings;
import com.gp3.model.RessourcesManager;

public class MainFrame extends JFrame
{
	private static final long serialVersionUID = 1L;

	public MainFrame()
	{
		setTitle(Constants.GAME_NAME + " " + Constants.VERSION);
		setIconImage(RessourcesManager.getPicture("favicon_128"));
		setMinimumSize(new Dimension(640, 420));
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		if (GameSettings.isFullScreen)
		{
			GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice dev = env.getDefaultScreenDevice();
			setResizable(false);
			setUndecorated(true);
			dev.setFullScreenWindow(this);
			dev.setDisplayMode(GameSettings.getResolutionForSize(GameSettings.currentWidth, GameSettings.currentHeight));
		}
		else
		{
			setSize(640, 420);
			setResizable(true);
			setLocationRelativeTo(null);
		}
		GameManager.getInstance().setMouseCursor(this);
		setVisible(true);
	}
}
