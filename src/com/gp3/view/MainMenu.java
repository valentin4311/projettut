package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.FloatControl;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;

import com.gp3.controller.EventHandler;
import com.gp3.model.AudioFile;
import com.gp3.model.ConsoleLogs;
import com.gp3.model.EnumDifficulty;
import com.gp3.model.EnumGameMode;
import com.gp3.model.Game;
import com.gp3.model.GameManager;
import com.gp3.model.GameSettings;
import com.gp3.model.Resolution;
import com.gp3.model.RessourcesManager;
import com.gp3.model.SaveManager;
import com.gp3.model.ScoresManager;

public class MainMenu extends JPanel implements GUI
{
	private static final long serialVersionUID = 1L;

	private static JPanel menuPanel;

	@Override
	public void initGUI()
	{
		setLayout(new BorderLayout());
		JPanel buttonPanel = new BackgroundPanel();
		{
			buttonPanel.setPreferredSize(new Dimension(150, 300));
			buttonPanel.setOpaque(false);

			JButton playButton = new JButton("Jouer");
			playButton.setFont(RenderHelper.COMIC_SANS_MS_18);
			playButton.addActionListener(new EventHandler(this, "playMenu"));

			JButton editorButton = new JButton("Editeur");
			editorButton.setFont(RenderHelper.COMIC_SANS_MS_18);
			editorButton.addActionListener(new EventHandler(this, "editorMenu"));

			JButton scoreButton = new JButton("Scores");
			scoreButton.setFont(RenderHelper.COMIC_SANS_MS_18);
			scoreButton.addActionListener(new EventHandler(this, "scoreMenu"));

			JButton settingsButton = new JButton("Options");
			settingsButton.setFont(RenderHelper.COMIC_SANS_MS_18);
			settingsButton.addActionListener(new EventHandler(this, "settingsMenu"));

			JButton quitButton = new JButton("Quitter");
			quitButton.setFont(RenderHelper.COMIC_SANS_MS_18);
			quitButton.addActionListener(new EventHandler(this, "quit"));

			GroupLayout gl_buttonPanel = new GroupLayout(buttonPanel);
			gl_buttonPanel.setHorizontalGroup(gl_buttonPanel.createParallelGroup(Alignment.LEADING).addGroup(gl_buttonPanel.createSequentialGroup().addContainerGap().addGroup(gl_buttonPanel.createParallelGroup(Alignment.LEADING).addComponent(quitButton, GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE).addComponent(settingsButton, GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE).addComponent(scoreButton, GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE).addComponent(editorButton, GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE).addComponent(playButton, GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)).addContainerGap()));
			gl_buttonPanel.setVerticalGroup(gl_buttonPanel.createParallelGroup(Alignment.TRAILING).addGroup(gl_buttonPanel.createSequentialGroup().addContainerGap(115, Short.MAX_VALUE).addComponent(playButton, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED).addComponent(editorButton, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED).addComponent(scoreButton, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED).addComponent(settingsButton, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED).addComponent(quitButton, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE).addContainerGap()));
			buttonPanel.setLayout(gl_buttonPanel);
		}
		menuPanel = new JPanel();
		{
			menuPanel.setBackground(null);
			menuPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
			menuPanel.setLayout(new BorderLayout());
			menuPanel.add(new IndexPanel(), BorderLayout.CENTER);
		}
		add(buttonPanel, BorderLayout.WEST);
		add(menuPanel, BorderLayout.CENTER);
	}

	@Override
	public void refreshGUI()
	{
		revalidate();
		repaint();
	}

	@Override
	public void handleAction(ActionEvent event, String buttonID)
	{
		//Menus principaux
		if (buttonID.equals("playMenu") && !(menuPanel.getComponents()[0] instanceof PlayPanel))
		{
			displayMenu(new PlayPanel(this), buttonID);
		}
		else if (buttonID.equals("editorMenu"))
		{
			JPanel oldPanel = (JPanel) menuPanel.getComponents()[0];
			if (oldPanel instanceof SettingsPanel)
				resetSettings();
			
			GameManager.getInstance().displayGUI(new MapChooser(true));
		}
		else if (buttonID.equals("scoreMenu") && !(menuPanel.getComponents()[0] instanceof ScoreboardPanel))
		{
			displayMenu(new ScoreboardPanel(this), buttonID);
		}
		else if (buttonID.equals("settingsMenu") && !(menuPanel.getComponents()[0] instanceof SettingsPanel))
		{
			displayMenu(new SettingsPanel(this), buttonID);
		}
		else if (buttonID.equals("emptyMenu") && !(menuPanel.getComponents()[0] instanceof IndexPanel))
		{
			displayMenu(new IndexPanel(), buttonID);
		}
		else if (buttonID.equals("quit"))
		{
			System.exit(0);
		}
		//Play menu
		else if (buttonID.equals("startNewGame"))
		{
			PlayPanel panel = (PlayPanel) menuPanel.getComponents()[0];
			EnumGameMode mode = panel.getSelectedGameMode();
			EnumDifficulty difficulty = panel.getSelectedDifficulty();

			if (mode != EnumGameMode.ADVENTURE)
			{
				GameManager.getInstance().displayGUI(new MapChooser(mode, difficulty));
			}
			else
			{
				new Game(mode, difficulty, null);
			}
		}
		else if (buttonID.equals("loadMenu") && !(menuPanel.getComponents()[0] instanceof LoadPanel))
		{
			displayMenu(new LoadPanel(this), buttonID);
		}
		//Load Menu
		else if(buttonID.equals("loadSave"))
		{
			int index = ((LoadPanel) menuPanel.getComponents()[0]).getSelectedIndex();
			if(index >= 0)
			{
				Game.loadMap(SaveManager.getSaveAt(index).getName());
				SaveManager.getSaveAt(index).delete();
			}
		}
		else if(buttonID.equals("deleteSave"))
		{
			int index = ((LoadPanel) menuPanel.getComponents()[0]).getSelectedIndex();
			if(index >= 0)
			{
				int result = JOptionPane.showInternalConfirmDialog(this, "Voulez-vous vraiment supprimer cette sauvegarde ?", "Confirmer Suppresion :", JOptionPane.YES_NO_OPTION);
				if(result == JOptionPane.YES_NO_OPTION)
				{
					SaveManager.getSaveAt(index).delete();
					((LoadPanel) menuPanel.getComponents()[0]).updateTable();
				}
			}
		}
		//Score
		else if (buttonID.equals("changeScoreboard"))
		{
			((ScoreboardPanel) menuPanel.getComponents()[0]).updateTable(((JComboBox<?>)event.getSource()).getSelectedIndex());
		}
		else if (buttonID.equals("clearScores"))
		{
			int result = JOptionPane.showInternalConfirmDialog(this, "Supprimer tous les scores pour le mode " + EnumGameMode.values()[((ScoreboardPanel) menuPanel.getComponents()[0]).getComboBox().getSelectedIndex()].toString(), "Supprimer les scores ?", JOptionPane.YES_NO_OPTION);
			if(result == JOptionPane.YES_OPTION)
			{
				ScoresManager.getInstance().clearScores(((ScoreboardPanel) menuPanel.getComponents()[0]).getComboBox().getSelectedIndex());
				((ScoreboardPanel) menuPanel.getComponents()[0]).updateTable(((ScoreboardPanel) menuPanel.getComponents()[0]).getComboBox().getSelectedIndex());
			}
		}
		//Settings
		else if (buttonID.equals("saveSettings"))
		{
			GameSettings.saveConfigFile();
			AudioFile.stopSettingMusic();
		}
		else if (buttonID.equals("refreshRessources"))
		{
			AudioFile.stopSettingMusic();
			ConsoleLogs.info("Reloading all ressources");
			RessourcesManager.loadAllRessources();
			((SettingsPanel) menuPanel.getComponents()[0]).refreshMusicBox();
			JOptionPane.showInternalMessageDialog(this, "Musiques recharg�es depuis le dossier \"./assets/\" !\nNote : Seul les fichiers au format \".wav\" sont accept�s !", "Ressources recharg�s !", JOptionPane.INFORMATION_MESSAGE);
		}
		else if (buttonID.equals("toggleFullscreen"))
		{
			((SettingsPanel) menuPanel.getComponents()[0]).getResolutionBox().setEnabled(!GameSettings.isFullScreen);
			GameManager.toggleFullScreen();
		}
		else if (buttonID.equals("changeStyle"))
		{
			String style = ((SettingsPanel) menuPanel.getComponents()[0]).getStyleBoxValue();
			GameSettings.gameStyle = style;
			GameManager.getInstance().setLAFFromName(style);
		}
		else if (buttonID.equals("changeMusic"))
		{
			AudioFile.stopSettingMusic();

			String music = ((SettingsPanel) menuPanel.getComponents()[0]).getMusicBoxValue();
			AudioFile audio = (AudioFile) RessourcesManager.getAudio(music + ".wav");
			if (audio != null)
			{
				audio.playMusic();
			}

			GameSettings.selectedMusic = music;
		}
		else if (buttonID.equals("changeResolution"))
		{
			Resolution resolution = ((SettingsPanel) menuPanel.getComponents()[0]).getResolutionBoxValue();
			GameSettings.currentWidth = resolution.getResolution().getWidth();
			GameSettings.currentHeight = resolution.getResolution().getHeight();
			GameManager.getInstance().setLAFFromName(((SettingsPanel) menuPanel.getComponents()[0]).getStyleBoxValue());
		}
		else if (buttonID.equals("resetSettings"))
		{
			AudioFile.stopSettingMusic();
			GameSettings.resetConfig();
			GameManager.getInstance().setLAFFromName(GameSettings.gameStyle);
		}
		else if (buttonID.equals("openMusicDir"))
		{
			try
			{
				Desktop.getDesktop().open(new File("./assets/audio/"));
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void destroyGUI()
	{}

	@Override
	public JPanel getPanel()
	{
		return this;
	}

	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		g.drawImage(RessourcesManager.getPicture("awesome logo"), 8, 8, 128, 128, null);
	}

	@Override
	public void handleChangeEvent(ChangeEvent event, String buttonID, JSlider slider)
	{
		if (buttonID.equals("soundVolume"))
		{
			GameSettings.soundVolume = slider.getValue();
		}
		else if (buttonID.equals("musicVolume"))
		{
			GameSettings.musicVolume = slider.getValue();
			AudioFile music = (AudioFile) RessourcesManager.getAudio(GameSettings.selectedMusic + ".wav");
			if (music != null)
			{
				FloatControl volume = (FloatControl) music.getClip().getControl(FloatControl.Type.MASTER_GAIN);
				volume.setValue(GameSettings.musicVolume == 0 ? -80 : 46 * GameSettings.musicVolume / 100F - 40);
			}
		}
	}

	public void resetSettings()
	{
		AudioFile music = (AudioFile) RessourcesManager.getAudio(GameSettings.selectedMusic + ".wav");
		if (music != null)
			music.stopMusic();
		GameSettings.loadConfigFile();
		GameManager.getInstance().setLAFFromName(GameSettings.gameStyle);
	}

	public void displayMenu(JPanel panel, String eventID)
	{
		JPanel oldPanel = (JPanel) menuPanel.getComponents()[0];

		menuPanel.removeAll();
		menuPanel.add(panel, BorderLayout.CENTER);
		refreshGUI();

		GameManager.getInstance().setCurrentMenu(eventID);

		if (oldPanel instanceof SettingsPanel)
			resetSettings();
	}

	public static JPanel getMenuPanel()
	{
		return menuPanel;
	}

	public static void setMenuPanel(JPanel menuPanel)
	{
		MainMenu.menuPanel = menuPanel;
	}
}
