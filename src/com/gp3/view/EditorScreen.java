package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.filechooser.FileFilter;

import com.gp3.controller.EventHandler;
import com.gp3.controller.MouseHandler;
import com.gp3.model.ConsoleLogs;
import com.gp3.model.EnumDifficulty;
import com.gp3.model.EnumGameMode;
import com.gp3.model.Game;
import com.gp3.model.GameManager;
import com.gp3.model.Location;
import com.gp3.model.Map;
import com.gp3.model.RessourcesManager;

public class EditorScreen extends JPanel implements GUI
{
	private static final long serialVersionUID = 1L;

	private MapPanel mainPanel;
	private Map editorMap;
	private PauseMenu pauseMenu;
	private JLabel mapName;
	private static JProgressBar mapProgress;
	private JFileChooser fc;
	private Random random = new Random();
	private String oldName = "";

	public EditorScreen(Map map)
	{
		if (map.getMapLocations().size() == 0)
		{
			return;
		}
		try
		{
			editorMap = new Map(map.getMapName() + ".kmap");

			for (Location loc : editorMap.getMapLocations())
			{
				loc.setEditorColor(new Color(random.nextInt(128) + 128, random.nextInt(128) + 128, random.nextInt(128) + 128));
			}
			
			oldName = editorMap.getMapName();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void initNewMap()
	{
		if (editorMap == null)
			editorMap = new Map();

		new Game(EnumGameMode.EDITOR, EnumDifficulty.EASY, editorMap);
	}

	public static boolean isMapCompleted()
	{
		return mapProgress.getValue() >= 100;
	}

	@Override
	public void initGUI()
	{
		initNewMap();
		setLayout(new BorderLayout());

		JPanel topPanel = new JPanel();
		{
			topPanel.setBorder(new EtchedBorder(EtchedBorder.RAISED));
			topPanel.setLayout(new BorderLayout());

			JButton menuButton = new JButton("Menu");
			menuButton.setPreferredSize(new Dimension(80, 28));
			menuButton.setFont(RenderHelper.COMIC_SANS_MS_14);
			menuButton.addActionListener(new EventHandler(this, "pauseMenu"));

			mapName = new JLabel("Carte");
			mapName.setHorizontalAlignment(SwingConstants.CENTER);
			mapName.setFont(RenderHelper.COMIC_SANS_MS_18);

			JButton helpButton = new JButton("Aide");
			helpButton.setPreferredSize(new Dimension(80, 28));
			helpButton.setFont(RenderHelper.COMIC_SANS_MS_14);
			helpButton.addActionListener(new EventHandler(this, "help"));

			topPanel.add(helpButton, BorderLayout.EAST);
			topPanel.add(mapName, BorderLayout.CENTER);
			topPanel.add(menuButton, BorderLayout.WEST);
		}
		mainPanel = new MapPanel();
		{
			mainPanel.addMouseListener(new MouseHandler());
			mainPanel.addMouseMotionListener(new MouseHandler());

			fc = new JFileChooser();
			fc.setVisible(false);
			fc.addActionListener(new EventHandler(this, "backgroundFile"));
			fc.setFileFilter(new FileFilter()
			{

				@Override
				public boolean accept(File file)
				{
					return file.isDirectory() || file.getName().substring(file.getName().length() - 4, file.getName().length()).equals(".png");
				}

				@Override
				public String getDescription()
				{
					return "Portable Network Graphics (PNG)";
				}

			});
		}

		JPanel bottomPanel = new JPanel();
		{
			bottomPanel.setBorder(new EtchedBorder(EtchedBorder.RAISED));
			bottomPanel.setLayout(new GridLayout(1, 2, 40, 0));

			JButton mapBackgroundButton = new JButton("Fond de la carte");
			mapBackgroundButton.setFont(RenderHelper.COMIC_SANS_MS_14);
			mapBackgroundButton.addActionListener(new EventHandler(this, "backgroundFile"));

			mapProgress = new JProgressBar();
			mapProgress.setString("Progression : " + mapProgress.getValue() + "%");
			mapProgress.setStringPainted(true);

			JButton mapNameButton = new JButton("Nom de la carte");
			mapNameButton.setFont(RenderHelper.COMIC_SANS_MS_14);
			mapNameButton.addActionListener(new EventHandler(this, "mapName"));

			bottomPanel.add(mapBackgroundButton);
			bottomPanel.add(mapProgress);
			bottomPanel.add(mapNameButton);
		}

		add(topPanel, BorderLayout.NORTH);
		add(mainPanel, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.SOUTH);
		pauseMenu = new PauseMenu(this);
		add(pauseMenu, BorderLayout.WEST);
	}

	@Override
	public void refreshGUI()
	{
		mapName.setText(editorMap.getMapName());
		int newProgressBarValue = 0;

		if (!editorMap.getMapName().equals("Nouvelle Carte"))
			newProgressBarValue += 10;
		if (editorMap.getMapImage() != RessourcesManager.getPicture("loadingScreen"))
			newProgressBarValue += 10;

		newProgressBarValue += 8 * Math.min(editorMap.getMapLocations().size(), 10);

		PauseMenu.updateButton();

		mapProgress.setValue(newProgressBarValue);
		mapProgress.setString(newProgressBarValue < 100 ? "Progression : " + mapProgress.getValue() + "%" : "Termin�");
		mainPanel.repaint();
	}

	@Override
	public void handleAction(ActionEvent event, String buttonID)
	{
		if (buttonID.equals("pauseMenu"))
		{
			pauseMenu.setVisible(!PauseMenu.isOpen());
		}
		else if (buttonID.equals("mapName"))
		{
			String newMapName = JOptionPane.showInternalInputDialog(this, "Entrez le nouveau nom de la carte :");

			if (newMapName != null)
			{
				if (RessourcesManager.getMap(newMapName) != null)
				{
					JOptionPane.showInternalMessageDialog(this, "Une carte avec le m�me nom existe d�j� !", "Nom incorrecte !", JOptionPane.ERROR_MESSAGE);
					handleAction(event, buttonID);
				}
				else if (newMapName.equalsIgnoreCase("Nouvelle Carte"))
				{
					JOptionPane.showInternalMessageDialog(this, "Le nom ne peut pas �tre \"Nouvelle Carte\"", "Nom incorrecte !", JOptionPane.ERROR_MESSAGE);
					handleAction(event, buttonID);
				}
				else if (newMapName.length() >= 4 && newMapName.length() <= 15)
				{
					editorMap.setMapName(newMapName);
					RessourcesManager.getAudio("s-signature.wav").playMusic();
					refreshGUI();
				}
				else
				{
					JOptionPane.showInternalMessageDialog(this, "Le nom doit comporter entre 4 et 15 carat�res", "Nom incorrecte !", JOptionPane.ERROR_MESSAGE);
					handleAction(event, buttonID);
				}
			}
		}
		else if (buttonID.equals("backgroundFile"))
		{
			if (fc.isVisible())
			{
				fc.setVisible(false);
				remove(fc);
				add(mainPanel);
				revalidate();
				repaint();

				File backgroundFile = fc.getSelectedFile();

				if (backgroundFile != null && backgroundFile.getName().substring(backgroundFile.getName().length() - 4, backgroundFile.getName().length()).equals(".png"))
				{
					try
					{
						editorMap.setMapImage(ImageIO.read(backgroundFile));
						refreshGUI();
					}
					catch (Exception e)
					{
						JOptionPane.showInternalMessageDialog(this, "Le fichier n'est pas au format .png ou est corrompu !", "Erreur de lecture", JOptionPane.ERROR_MESSAGE);
						e.printStackTrace();
					}
				}
			}
			else
			{
				fc.setVisible(true);
				remove(mainPanel);
				add(fc);
				revalidate();
				repaint();
			}
		}
		else if (buttonID.equals("help"))
		{
			JOptionPane.showInternalMessageDialog(this, "Bienvenue sur l'�diteur !\n\nListe des commandes :\nAjouter une zone :\n  - Clic gauche : Ajouter un point � la zone\n  - Clic droit valider la zone\n\n Configurer une zone / point : Clic droit.\n\nNotes : Une zone constitu�e d'un point est consid�r� comme tel.\nAttention ! Ne regroupez pas les points de d�but et de fin de zone.\nEffectuez un clique droit pour valider la zone.", "Aide �diteur :", JOptionPane.INFORMATION_MESSAGE);
		}
		else if (buttonID.equals("closeMenu"))
		{
			pauseMenu.dispose();
		}
		else if (buttonID.equals("saveGame"))
		{
			if(Map.isOfficial(oldName))
			{
				if(editorMap.isOfficial())
				{
					JOptionPane.showInternalMessageDialog(this, "Afin de sauvegarder un changement sur une carte officielle, vous devez la renommer.", "Sauvegarde impossible :", JOptionPane.ERROR_MESSAGE);
					return;
				}
			}
			else
			{
				RessourcesManager.getRessourceList().remove("./map/" + oldName + ".kmap");
				new File("./assets/map/" + oldName + ".kmap").delete();
			}
			
			ConsoleLogs.info("Saving " + editorMap.getMapName() + ".kmap...");
			try
			{
				editorMap.saveMap();
				ConsoleLogs.info("Done");
				JOptionPane.showInternalMessageDialog(this, "Le fichier a �t� sauvegard� dans \"assets/map/" + editorMap.getMapName() + ".kmap\"", "Sauvegarde termin�e :", JOptionPane.INFORMATION_MESSAGE);
				RessourcesManager.addMap(editorMap.getMapName() + ".kmap");
			}
			catch (Exception e)
			{
				ConsoleLogs.error("Failed to save " + editorMap.getMapName() + " !");
				e.printStackTrace();
			}
		}
		else if (buttonID.equals("surrender"))
		{
			int result = JOptionPane.showInternalConfirmDialog(this, "Voulez-vous vraiment retourner � l'�cran titre ?", "Retourner � l'�cran titre ?", JOptionPane.YES_NO_OPTION);

			if (result == JOptionPane.YES_OPTION)
			{
				pauseMenu.dispose();
				GameManager.getInstance().displayGUI(new MainMenu());
				GameManager.getInstance().setCurrentGame(null);
			}
		}
		else if (buttonID.equals("quitGame"))
		{
			int result = JOptionPane.showInternalConfirmDialog(this, "Voulez-vous vraiment quitter le jeu ?", "Quitter le jeu ?", JOptionPane.YES_NO_OPTION);

			if (result == JOptionPane.YES_OPTION)
			{
				System.exit(0);
			}
		}
	}

	@Override
	public void handleChangeEvent(ChangeEvent event, String buttonID, JSlider slider){}

	@Override
	public void destroyGUI(){}

	@Override
	public JPanel getPanel()
	{
		return this;
	}

}
