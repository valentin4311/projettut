package com.gp3.view;

import javax.swing.table.DefaultTableModel;

public class NonEditableTable extends DefaultTableModel
{
	private static final long serialVersionUID = 1L;

	public NonEditableTable(Object[][] data, Object[] header)
	{
		super(data, header);
	}

	@Override
	public boolean isCellEditable(int row, int column)
	{
		return false;
	}
}
