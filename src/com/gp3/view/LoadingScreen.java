package com.gp3.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

public class LoadingScreen extends JFrame implements GUI
{
	private static final long serialVersionUID = 1L;
	private final LoadingPanel loadingPanel = new LoadingPanel();

	@Override
	public void initGUI()
	{
		setSize(new Dimension(560, 335));
		setResizable(false);
		setUndecorated(true);
		setLocationRelativeTo(null);
		setContentPane(loadingPanel);
		setVisible(true);
	}

	@Override
	public void refreshGUI()
	{}

	@Override
	public void handleAction(ActionEvent event, String buttonID)
	{}

	@Override
	public void destroyGUI()
	{
		dispose();
	}

	@Override
	public JPanel getPanel()
	{
		return loadingPanel;
	}

	@Override
	public void handleChangeEvent(ChangeEvent event, String buttonID, JSlider slider)
	{}
}
