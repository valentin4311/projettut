package com.gp3.view;

import java.awt.event.ActionEvent;

import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

public interface GUI
{
	/**
	 * Used to create the gui
	 */
	public void initGUI();

	/**
	 * Used to refresh elements
	 */
	public void refreshGUI();

	/**
	 * Used to handle an action on an element
	 * @param event
	 * @param buttonID
	 */
	public void handleAction(ActionEvent event, String buttonID);

	public void handleChangeEvent(ChangeEvent event, String buttonID, JSlider slider);

	public void destroyGUI();

	public JPanel getPanel();
}
