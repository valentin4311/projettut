package com.gp3.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Point2D;

import javax.swing.JPanel;

import com.gp3.controller.MouseHandler;
import com.gp3.model.EnumGameMode;
import com.gp3.model.Game;
import com.gp3.model.GameManager;
import com.gp3.model.Location;
import com.gp3.model.RessourcesManager;

public class MapPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	public MapPanel()
	{
		MouseHandler.getCoordinatesList().clear();
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Game game = GameManager.getInstance().getCurrentGame();

		g.drawImage(game.getCurrentMap().getMapImage(), 0, 0, getWidth(), getHeight(), null);
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(3));

		if (game.getGamemode() == EnumGameMode.EDITOR)
		{
			for (Location loc : game.getCurrentMap().getMapLocations())
			{
				if (!loc.isPoint())
				{
					Polygon polygon = new Polygon();

					for (Point2D.Float point : loc.getArea())
					{
						polygon.addPoint((int) (point.x * getWidth()), (int) (point.y * getHeight()));
					}
					g.setColor(new Color(loc.getEditorColor().getRed(), loc.getEditorColor().getGreen(), loc.getEditorColor().getBlue(), 128));
					g.fillPolygon(polygon);
					g.setColor(loc.getEditorColor());
					g.drawPolygon(polygon);
				}
			}
		}

		for (Location loc : game.getCurrentMap().getMapLocations())
		{
			if (loc.isPoint())
			{
				g.drawImage(RessourcesManager.getPicture("point"), (int) (loc.getArea()[0].getX() * getWidth()) - 8, (int) (loc.getArea()[0].getY() * getHeight()) - 8, 16, 16, null);
			}
		}
		if (game.getGamemode() == EnumGameMode.EDITOR)
		{
			if (!MouseHandler.getCoordinatesList().isEmpty())
			{
				Polygon pol = new Polygon();
				for (Point2D.Float loc : MouseHandler.getCoordinatesList())
				{
					pol.addPoint((int) (loc.getX() * getWidth()), (int) (loc.getY() * getHeight()));
				}
				pol.addPoint(MouseHandler.getMouseX(), MouseHandler.getMouseY());

				g.setColor(new Color(MouseHandler.getCurrentPolygonColor().getRed(), MouseHandler.getCurrentPolygonColor().getGreen(), MouseHandler.getCurrentPolygonColor().getBlue(), 128));
				g.fillPolygon(pol);
				g.setColor(MouseHandler.getCurrentPolygonColor());
				g.drawPolygon(pol);

				for (Point2D.Float loc : MouseHandler.getCoordinatesList())
				{
					g.drawImage(RessourcesManager.getPicture("point2"), (int) (loc.x * getWidth()) - 8, (int) (loc.y * getHeight()) - 8, 16, 16, null);
				}
			}

			if (MouseHandler.isIn())
			{
				g.drawImage(RessourcesManager.getPicture("point2"), MouseHandler.getMouseX() - 8, MouseHandler.getMouseY() - 8, 16, 16, null);
			}
		}
	}
}
