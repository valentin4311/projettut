package com.gp3.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;

import com.gp3.controller.ChangeHandler;
import com.gp3.controller.EventHandler;
import com.gp3.model.EnumDifficulty;
import com.gp3.model.EnumGameMode;
import com.gp3.model.Game;
import com.gp3.model.GameManager;
import com.gp3.model.Map;
import com.gp3.model.RessourcesManager;

public class MapChooser extends JPanel implements GUI
{
	private static final long serialVersionUID = 1L;

	private MapOverviewPanel currentMap;
	private JLabel mapNameLabel;
	private JSlider slider;

	private EnumGameMode mode;
	private EnumDifficulty diff;
	private boolean editorMode = false;

	public MapChooser(boolean editor)
	{
		editorMode = editor;
	}

	public MapChooser(EnumGameMode mode, EnumDifficulty difficulty)
	{
		editorMode = false;
		this.mode = mode;
		diff = difficulty;
	}

	@Override
	public void initGUI()
	{
		setBorder(new EmptyBorder(4, 0, 0, 0));
		setLayout(new BorderLayout());

		JLabel titleLabel = new JLabel("Choix de la carte");
		titleLabel.setFont(RenderHelper.COMIC_SANS_MS_18);
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);

		JPanel centerPanel = new JPanel();
		{
			centerPanel.setBorder(new EmptyBorder(4, 15, 15, 15));
			centerPanel.setLayout(new BorderLayout());

			currentMap = new MapOverviewPanel();
			currentMap.setOverviewMap(RessourcesManager.getMapList(editorMode).get(0));
			currentMap.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
			centerPanel.add(currentMap);
		}

		JPanel southPanel = new JPanel();
		{
			southPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
			southPanel.setLayout(new BorderLayout());

			JPanel buttonPanel = new JPanel();
			{
				buttonPanel.setLayout(new GridLayout(1, 3, 20, 0));
				buttonPanel.setBorder(new EmptyBorder(4, 4, 4, 4));

				JButton chooseButton = new JButton("Selectionner");
				chooseButton.addActionListener(new EventHandler(this, "selectButton"));

				JButton deleteButton = new JButton("Supprimer");
				deleteButton.addActionListener(new EventHandler(this, "deleteButton"));

				JButton backButton = new JButton("Retour");
				backButton.addActionListener(new EventHandler(this, "backButton"));

				buttonPanel.add(chooseButton);
				buttonPanel.add(deleteButton);
				buttonPanel.add(backButton);
			}
			JPanel mapChooserPanel = new JPanel();
			{
				mapChooserPanel.setLayout(new BorderLayout());

				mapNameLabel = new JLabel(RessourcesManager.getMapList(editorMode).get(0).getMapName());
				mapNameLabel.setFont(RenderHelper.COMIC_SANS_MS_14);
				mapNameLabel.setHorizontalAlignment(SwingConstants.CENTER);

				ArrayList<Map> mapList = RessourcesManager.getMapList(editorMode);
				slider = new JSlider();
				slider.setValue(0);
				slider.setMaximum(mapList.size() - 1);
				slider.setPaintLabels(true);
				slider.setPaintTicks(true);
				slider.setMinorTickSpacing(1);
				slider.setMajorTickSpacing(1);
				Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
				int firstMap = editorMode ? 1 : 0;
				if (editorMode)
				{
					labelTable.put(0, new JLabel("+"));
				}
				for(int i = firstMap; i < mapList.size();i++)
				{
					labelTable.put(i, new JLabel(mapList.get(i).getMapName().substring(0, 1)));
				}
				slider.setLabelTable(labelTable);
				slider.addChangeListener(new ChangeHandler(this, "mapChange", slider));

				mapChooserPanel.add(mapNameLabel, BorderLayout.NORTH);
				mapChooserPanel.add(slider, BorderLayout.SOUTH);
			}

			southPanel.add(mapChooserPanel, BorderLayout.NORTH);
			southPanel.add(buttonPanel, BorderLayout.SOUTH);
		}

		add(titleLabel, BorderLayout.NORTH);
		add(centerPanel, BorderLayout.CENTER);
		add(southPanel, BorderLayout.SOUTH);
	}

	@Override
	public void refreshGUI()
	{}

	@Override
	public void handleAction(ActionEvent event, String buttonID)
	{
		if (buttonID.equals("backButton"))
		{
			if (editorMode)
			{
				GameManager.getInstance().displayGUI(new MainMenu());
			}
			else
			{
				MainMenu menu = new MainMenu();
				GameManager.getInstance().displayGUI(menu);
				menu.handleAction(null, "playMenu");
			}
		}
		else if (buttonID.equals("deleteButton"))
		{
			Map map = RessourcesManager.getMapList(editorMode).get(slider.getValue());
			
			if(map.isOfficial() || map.getMapLocations().isEmpty())
			{
				JOptionPane.showInternalMessageDialog(this, "Vous ne pouvez pas supprimer une carte officielle.", "Action impossible !", JOptionPane.ERROR_MESSAGE);
				return;
			}
			int result = JOptionPane.showInternalConfirmDialog(this, "Voulez-vous supprimer la carte \"" + currentMap.getOverviewMap().getMapName() + "\"", "Supprimer ?", JOptionPane.YES_OPTION);

			if (result == JOptionPane.YES_OPTION)
			{
				RessourcesManager.getRessourceList().remove("./map/" + map.getMapName() + ".kmap");
				new File("./assets/map/" + map.getMapName() + ".kmap").delete();
				removeAll();
				initGUI();
				revalidate();
			}
		}
		else if (buttonID.equals("selectButton"))
		{
			Map map = RessourcesManager.getMapList(editorMode).get(slider.getValue());

			if (editorMode)
			{
				GameManager.getInstance().displayGUI(new EditorScreen(map));
			}
			else
			{
				new Game(mode, diff, map);
			}
		}
	}

	@Override
	public void handleChangeEvent(ChangeEvent event, String buttonID, JSlider slider)
	{
		if (buttonID.equals("mapChange"))
		{
			Map map = RessourcesManager.getMapList(editorMode).get(slider.getValue());
			mapNameLabel.setText(map.getMapName());
			currentMap.setOverviewMap(map);
			currentMap.repaint();
		}
	}

	@Override
	public void destroyGUI()
	{}

	@Override
	public JPanel getPanel()
	{
		return this;
	}
}
