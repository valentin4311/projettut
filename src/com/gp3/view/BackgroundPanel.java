package com.gp3.view;

import java.awt.Graphics;

import javax.swing.JPanel;

import com.gp3.model.RessourcesManager;

public class BackgroundPanel extends JPanel
{
	private static final long serialVersionUID = 1L;

	@Override
	public void paint(Graphics g)
	{
		g.drawImage(RessourcesManager.getPicture("background"), 0, 0, getWidth(), getHeight(), null);
		super.paint(g);
	}
}
