package com.gp3.view;

import java.awt.Font;

public class RenderHelper
{
	public static final Font COMIC_SANS_MS_22 = new Font("Comic Sans MS", Font.PLAIN, 22);
	public static final Font COMIC_SANS_MS_18 = new Font("Comic Sans MS", Font.PLAIN, 18);
	public static final Font COMIC_SANS_MS_16 = new Font("Comic Sans MS", Font.PLAIN, 16);
	public static final Font COMIC_SANS_MS_14 = new Font("Comic Sans MS", Font.PLAIN, 14);
}
