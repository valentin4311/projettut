package com.gp3.model;

import java.awt.Image;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class Map implements Comparable<Map>
{
	private String mapName;
	private ArrayList<Location> mapLocations = new ArrayList<Location>();
	private Image mapImage;

	public Map()
	{
		mapImage = RessourcesManager.getPicture("loadingScreen");
		mapName = "Nouvelle Carte";
	}

	public Map(String filePath) throws Exception
	{
		loadMap(filePath);
	}

	private void loadMap(String path) throws Exception
	{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
		Document xml = docBuilder.parse(new File("./assets/map/" + path));
		xml.getDocumentElement().normalize();

		mapName = xml.getElementsByTagName("mapName").item(0).getChildNodes().item(0).getNodeValue();

		NodeList locationList = xml.getElementsByTagName("location");
		for (int i = 0; i < locationList.getLength(); i++)
		{
			Node node = locationList.item(i);

			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;

				String locationName = element.getElementsByTagName("name").item(0).getChildNodes().item(0).getNodeValue();

				NodeList pointList = element.getElementsByTagName("point");
				Point2D.Float[] pointArray = new Point2D.Float[pointList.getLength()];

				for (int j = 0; j < pointList.getLength(); j++)
				{
					Element point = (Element) pointList.item(j);
					String[] points = point.getChildNodes().item(0).getNodeValue().split(",");
					pointArray[j] = new Point2D.Float(Float.valueOf(points[0]), Float.valueOf(points[1]));
				}

				mapLocations.add(new Location(locationName, pointArray));
			}
		}
		BASE64Decoder decoder = new BASE64Decoder();
		ByteArrayInputStream bis = new ByteArrayInputStream(decoder.decodeBuffer(xml.getElementsByTagName("binary").item(0).getChildNodes().item(0).getNodeValue()));
		mapImage = ImageIO.read(bis);
	}

	public void saveMap() throws Exception
	{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();

		Element root = doc.createElement("map");
		doc.appendChild(root);

		Node mapName = doc.createElement("mapName");
		mapName.appendChild(doc.createTextNode(getMapName()));
		root.appendChild(mapName);

		for (Location loc : getMapLocations())
		{
			Element location = doc.createElement("location");
			root.appendChild(location);
			Element locationName = doc.createElement("name");
			location.appendChild(locationName);
			locationName.appendChild(doc.createTextNode(loc.getLocationName()));

			for (Point2D.Float point : loc.getArea())
			{
				Element coords = doc.createElement("point");
				location.appendChild(coords);
				coords.appendChild(doc.createTextNode(point.getX() + "," + point.getY()));
			}
		}

		Element image = doc.createElement("binary");
		root.appendChild(image);

		BASE64Encoder encoder = new BASE64Encoder();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write((BufferedImage) mapImage, "png", bos);
		image.appendChild(doc.createTextNode(encoder.encode(bos.toByteArray())));

		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		Result output = new StreamResult(new File("./assets/map/" + getMapName() + ".kmap"));
		Source input = new DOMSource(doc);
		transformer.transform(input, output);
	}

	public String getMapName()
	{
		return mapName;
	}

	public void setMapName(String name)
	{
		mapName = name;
	}

	public Image getMapImage()
	{
		return mapImage;
	}

	public void setMapImage(Image image)
	{
		mapImage = image;
	}

	public ArrayList<Location> getMapLocations()
	{
		return mapLocations;
	}

	public boolean isOfficial()
	{
		return isOfficial(mapName);
	}
	
	public static boolean isOfficial(String name)
	{
		return name.equals("France") || name.equals("Mayenne") || name.equals("IUT de Laval") || name.equals("D�p. Info") || name.equals("Laval");
	}
	
	@Override
	public int compareTo(Map map)
	{
		return map == null ? 1 : getMapName().compareToIgnoreCase(map.mapName);
	}

	@Override
	public boolean equals(Object obj)
	{
		return obj instanceof Map && ((Map) obj).mapName.equals(mapName);
	}

	@Override
	public int hashCode()
	{
		return mapName.hashCode();
	}
}
