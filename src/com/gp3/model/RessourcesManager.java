package com.gp3.model;

import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class RessourcesManager
{
	private static HashMap<String, Object> ressourceList = new HashMap<String, Object>();

	public static void loadAllRessources()
	{
		loadFolderFiles(new File("./assets"));
	}

	public static String[] getAllMusic()
	{
		ArrayList<String> musicList = new ArrayList<String>();
		musicList.add("Aucune");
		musicList.add("Aleatoire");

		for (String ressources : ressourceList.keySet())
		{
			if (ressources.startsWith("./audio/") && !ressources.contains("s-"))
			{
				musicList.add(ressources.substring(8, ressources.length() - 4));
			}
		}
		String[] array = new String[musicList.size()];
		musicList.toArray(array);
		return array;
	}

	public static ArrayList<Map> getMapList(boolean editor)
	{
		ArrayList<Map> mapList = new ArrayList<Map>();
		if (editor)
		{
			mapList.add(null);
		}
		for (Object ressource : ressourceList.values())
		{
			if (ressource instanceof Map)
			{
				mapList.add((Map) ressource);
			}
		}
		Collections.sort(mapList);
		if (editor)
		{
			mapList.set(0, new Map());
		}
		return mapList;
	}

	public static void addAudio(String url)
	{
		File file = new File("./assets/audio/" + url);
		try
		{
			if (!ressourceList.containsKey("./audio/" + url))
			{
				AudioFile audio = new AudioFile("./assets/audio/" + url);
				ressourceList.put("./audio/" + url, audio);
				ConsoleLogs.info("File " + url + " loaded succesfully");
			}
		}
		catch (Exception e)
		{
			ConsoleLogs.error("Unable to load " + file.getAbsolutePath());
			e.printStackTrace();
		}
	}

	public static void addPicture(String url)
	{
		File file = new File("./assets/pictures/" + url);
		try
		{
			if (!ressourceList.containsKey("./pictures/" + url))
			{
				Image img = ImageIO.read(file);
				ressourceList.put("./pictures/" + url, img);
				ConsoleLogs.info("File " + url + " loaded succesfully");
			}
		}
		catch (Exception e)
		{
			ConsoleLogs.error("Unable to load " + file.getAbsolutePath());
			e.printStackTrace();
		}
	}

	public static void addMap(String url)
	{
		File file = new File("./assets/map/" + url);
		try
		{
			Map map = new Map(url);
			if (!ressourceList.containsKey("./map/" + url))
			{
				ressourceList.put("./map/" + url, map);
				ConsoleLogs.info("Map " + url + " loaded succesfully");
			}
			else
			{
				ressourceList.put("./map/" + url, map);
				ConsoleLogs.info("Map " + url + " reloaded succesfully");
			}
		}
		catch (Exception e)
		{
			ConsoleLogs.error("Unable to load " + file.getAbsolutePath());
			e.printStackTrace();
		}
	}

	public static Image getPicture(String url)
	{
		Object ressource = ressourceList.get("./pictures/" + url + ".png");

		if (ressource instanceof Image)
		{
			return (Image) ressource;
		}
		return null;
	}

	public static AudioFile getAudio(String url)
	{
		Object ressource = ressourceList.get("./audio/" + url);

		if (ressource instanceof AudioFile)
		{
			return (AudioFile) ressource;
		}
		return null;
	}

	public static Map getMap(String url)
	{
		Object ressource = ressourceList.get("./map/" + url + ".kmap");

		if (ressource instanceof Map)
		{
			return (Map) ressource;
		}
		return null;
	}

	private static void loadFolderFiles(File file)
	{
		File[] fileList = file.listFiles();

		for (File currentFile : fileList)
		{
			if (currentFile.isFile())
			{
				String[] fileName = currentFile.getName().split("\\.");
				String extension = fileName[fileName.length - 1];

				if (extension.equals("png"))
				{
					addPicture(currentFile.getName());
				}
				else if (extension.equals("wav"))
				{
					addAudio(currentFile.getName());
				}
				else if (extension.equals("kmap"))
				{
					addMap(currentFile.getName());
				}
			}
			else if (currentFile.isDirectory())
			{
				loadFolderFiles(currentFile);
			}
		}
	}

	public static HashMap<String, Object> getRessourceList()
	{
		return ressourceList;
	}
}
