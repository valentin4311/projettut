package com.gp3.model;

public enum EnumGameMode
{
	ADVENTURE("Aventure"), ARCADE("Arcade"), SURVIVAL("Survie"), ZEN("Zen"), EDITOR("Editeur");
	
	private String name;
	
	private EnumGameMode(String name)
	{
		this.name = name;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
}
