package com.gp3.model;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class AudioFile
{
	private Clip clip;
	private AudioInputStream audioStream;
	private boolean isSound = false;

	public AudioFile(String url) throws Exception
	{
		isSound = url.contains("s-");
		InputStream in = new BufferedInputStream(new FileInputStream(url));
		audioStream = AudioSystem.getAudioInputStream(in);
		clip = AudioSystem.getClip();
		clip.open(audioStream);
	}

	public void playMusic()
	{
		clip.setFramePosition(0);
		FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
		volume.setValue(isSound ? GameSettings.soundVolume == 0 ? -80 : 46 * GameSettings.soundVolume / 100F - 40 : GameSettings.musicVolume == 0 ? -80 : 46 * GameSettings.musicVolume / 100F - 40);
		clip.start();
	}

	public void loop()
	{
		clip.setFramePosition(0);
		FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
		volume.setValue(isSound ? GameSettings.soundVolume == 0 ? -80 : 46 * GameSettings.soundVolume / 100F - 40 : GameSettings.musicVolume == 0 ? -80 : 46 * GameSettings.musicVolume / 100F - 40);
		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}

	public void stopMusic()
	{
		clip.stop();
	}

	public Clip getClip()
	{
		return clip;
	}

	public static void stopSettingMusic()
	{
		AudioFile old = (AudioFile) RessourcesManager.getAudio(GameSettings.selectedMusic + ".wav");
		if (old != null)
			old.stopMusic();
	}
}
