package com.gp3.model;

import java.awt.DisplayMode;

public class Resolution
{
	private DisplayMode resolution;

	public DisplayMode getResolution()
	{
		return resolution;
	}

	public Resolution(DisplayMode mode)
	{
		resolution = mode;
	}

	@Override
	public String toString()
	{
		return resolution.getWidth() + "x" + resolution.getHeight();
	}
}
