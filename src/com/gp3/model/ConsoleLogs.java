package com.gp3.model;

public class ConsoleLogs
{
	public static void info(String arg)
	{
		System.out.println("[INFO]" + arg);
	}

	public static void warning(String arg)
	{
		System.out.println("[WARNING]" + arg);
	}

	public static void error(String arg)
	{
		System.err.println("[ERROR]" + arg);
	}
}
