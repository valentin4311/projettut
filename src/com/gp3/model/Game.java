package com.gp3.model;

import java.awt.Polygon;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.gp3.view.DialogPanel;
import com.gp3.view.IngameScreen;
import com.gp3.view.MainMenu;
import com.gp3.view.ScoreboardPanel;

public class Game implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Random RANDOM = new Random();

	private final EnumGameMode gamemode;
	private final EnumDifficulty difficulty;

	private transient Map currentMap;
	private String mapName;
	private Location targetLocation;

	private int maxQuestion;
	private int currentQuestion = 0;
	private int score = 0;

	private int timeLeft;
	private int maxTime = 10000;

	private int totalTime = 15000;
	private int maxTotalTime = 50000;

	private int lifeAmount;

	private Random random = new Random();
	private transient AudioFile backgroundMusic;
	
	private Story story;
	
	public Game(EnumGameMode gamemode, EnumDifficulty difficulty, Map map)
	{
		this.gamemode = gamemode;
		this.difficulty = difficulty;
		if(map != null)
		{
			this.currentMap = map;
			mapName = currentMap.getMapName();
		}

		GameManager.getInstance().setCurrentGame(this);
		prepareBackgroundMusic();

		if (gamemode != EnumGameMode.EDITOR)
		{
			if (gamemode == EnumGameMode.ADVENTURE)
			{
				story = TheGreatStoryBook.tellMeAStory();
				
				currentMap = story.getCurrentMap();
				mapName = currentMap.getMapName();
				
				lifeAmount = difficulty == EnumDifficulty.EASY ? 5 : difficulty == EnumDifficulty.MEDIUM ? 3 : difficulty == EnumDifficulty.HARD ? 2 : 1;
				GameManager.getInstance().displayGUI(new DialogPanel(story.getStory().getStoryStart(), 0));
				return;
			}
			else if(gamemode == EnumGameMode.SURVIVAL)
			{
				lifeAmount = difficulty == EnumDifficulty.EASY || difficulty == EnumDifficulty.MEDIUM ? 3 : difficulty == EnumDifficulty.HARD ? 2 : 1;
			}
			
			GameManager.getInstance().displayGUI(new IngameScreen());
			initGame();
		}
	}
	
	public void initStoryGame()
	{

		String storyTarget = story.getCurrentEnd();
		
		int targetLocationID = 0;
		Location location = null;
		
		for(Location loc : currentMap.getMapLocations())
		{
			if(loc.getLocationName().equalsIgnoreCase(storyTarget))
			{
				location = loc;
				break;
			}
			targetLocationID++;
		}
		
		currentMap.getMapLocations().remove(targetLocationID);
		
		Collections.shuffle(currentMap.getMapLocations());
		
		currentMap.getMapLocations().add(location);
		
		setMaxQuestion(Math.min(RANDOM.nextInt(15) + 10, currentMap.getMapLocations().size()));
		
		targetLocation = currentMap.getMapLocations().get(0);
		currentQuestion = 0;
		
		IngameScreen.setQuestionLabel(0, maxQuestion);
		IngameScreen.setQuestLabel(targetLocation.getLocationName());
		IngameScreen.updateScoreLabel();

		setTimeLeft(getMaxTime());
		TimerThread timer = new TimerThread();
		timer.start();
	}

	public void initGame()
	{
		Collections.shuffle(currentMap.getMapLocations());
		targetLocation = currentMap.getMapLocations().get(0);
		currentQuestion = 0;
		setMaxQuestion(Math.min(RANDOM.nextInt(15) + 10, currentMap.getMapLocations().size()));
		IngameScreen.setQuestionLabel(0, maxQuestion);
		IngameScreen.setQuestLabel(targetLocation.getLocationName());
		IngameScreen.updateScoreLabel();

		if (gamemode == EnumGameMode.ARCADE || gamemode == EnumGameMode.SURVIVAL || gamemode == EnumGameMode.ADVENTURE)
		{
			setTimeLeft(getMaxTime());
			TimerThread timer = new TimerThread();
			timer.start();
		}
	}

	public void answerAt(int x, int y, JPanel panel)
	{
		if (panel != null)
		{
			Polygon polygon = new Polygon();
			for (Point2D.Float point : targetLocation.getArea())
			{
				polygon.addPoint((int) ((float) point.getX() * (float) panel.getWidth()), (int) ((float) point.getY() * (float) panel.getHeight()));
			}

			if (polygon.npoints > 1)
			{
				if (polygon.contains(x, y))
				{
					correctAnswer(-1);
				}
				else
				{
					badAnswer();
				}
			}
			else
			{
				Point2D.Float point = targetLocation.getArea()[0];
				int distance = (int) Math.sqrt(Math.pow(x - point.x * panel.getWidth(), 2) + Math.pow((y - point.y * panel.getHeight()), 2));

				if (distance <= 30)
				{
					correctAnswer(distance);
				}
				else
				{
					badAnswer();
				}
			}
		}
		else
		{
			badAnswer();
		}

		if (currentQuestion >= maxQuestion - 1)
		{
			winGame();
			return;
		}

		setCurrentQuestion(getCurrentQuestion() + 1);
		setCurrentTarget(currentQuestion);

		if(getCurrentQuestion() == getMaxQuestion() - 1 && getGamemode() == EnumGameMode.ADVENTURE)
		{
			setCurrentTarget(currentMap.getMapLocations().size() - 1);
		}
		
		if (gamemode == EnumGameMode.ARCADE || gamemode == EnumGameMode.SURVIVAL || gamemode == EnumGameMode.ADVENTURE)
		{
			setTimeLeft(getMaxTime());
		}
	}

	public void badAnswer()
	{
		RessourcesManager.getAudio("s-error.wav").playMusic();
		if (gamemode == EnumGameMode.SURVIVAL)
		{
			setLifeAmount(getLifeAmount() - 1);
			addScore(-50);
		}
		else if (gamemode == EnumGameMode.ADVENTURE)
		{
			//setLifeAmount(getLifeAmount() - 1);
		}
		else if (gamemode == EnumGameMode.ARCADE)
		{
			setTotalTime(Math.max(getTotalTime() - (int) (5000 * difficulty.getTimeMultiplier()), 0));
		}
	}

	public void correctAnswer(int distance)
	{
		RessourcesManager.getAudio("s-correct.wav").playMusic();
		if (distance == -1)
		{
			addScore(30);
		}
		else
		{
			addScore(30 - (distance <= 9 ? 0 : distance));
		}

		if (gamemode == EnumGameMode.ARCADE)
		{
			setTotalTime(Math.min(getTimeLeft() + getTotalTime(), getMaxTotalTime()));
		}
	}

	public void winAdventure()
	{
		score += 1000;
		RessourcesManager.getAudio("s-win.wav").playMusic();
		TimerThread.endTimer();
		
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				MainMenu menu = new MainMenu();
				GameManager.getInstance().displayGUI(menu);
				menu.displayMenu(new ScoreboardPanel(menu), "scoreMenu");
				GameManager.getInstance().setCurrentGame(null);
				JOptionPane.showInternalMessageDialog(menu, "Vous avez gagn� !", "F�licitations !", JOptionPane.PLAIN_MESSAGE);
				
				String name = null;
				
				while(name == null)
				{
					name = JOptionPane.showInternalInputDialog(menu, "Entrez votre nom :");
					
					if(name == null || (name != null && name.length() < 3 || name.length() > 16))
					{
						name = null;
						JOptionPane.showInternalMessageDialog(menu, "Le nom doit comporter entre 3 et 16 caract�res !", "Nom invalide !", JOptionPane.ERROR_MESSAGE);
					}
				}
				
				ScoresManager.getInstance().addScore(name, score, getDifficulty(), getGamemode());
				GameManager.getInstance().getCurrentGUI().getPanel().repaint();
				((ScoreboardPanel) MainMenu.getMenuPanel().getComponents()[0]).updateTable(0);
			}
		});
	}
	
	public void looseGame()
	{
		RessourcesManager.getAudio("s-gameover.wav").playMusic();
		TimerThread.endTimer();
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				MainMenu menu = new MainMenu();
				GameManager.getInstance().displayGUI(menu);
				menu.displayMenu(new ScoreboardPanel(menu), "scoreMenu");
				GameManager.getInstance().setCurrentGame(null);
				JOptionPane.showInternalMessageDialog(menu, "Vous avez perdu !", "Game Over !", JOptionPane.PLAIN_MESSAGE);
				
				String name = null;
				
				while(name == null)
				{
					name = JOptionPane.showInternalInputDialog(menu, "Entrez votre nom :");
					
					if(name == null || (name != null && name.length() < 3 || name.length() > 16))
					{
						name = null;
						JOptionPane.showInternalMessageDialog(menu, "Le nom doit comporter entre 3 et 16 caract�res !", "Nom invalide !", JOptionPane.ERROR_MESSAGE);
					}
				}
				
				ScoresManager.getInstance().addScore(name, score, getDifficulty(), getGamemode());
				((ScoreboardPanel) MainMenu.getMenuPanel().getComponents()[0]).updateTable(0);
			}
		});
	}

	public void winGame()
	{
		TimerThread.endTimer();
		if(gamemode == EnumGameMode.ADVENTURE)
		{
			currentMap = story.nextMap();
			if(currentMap != null)
			{
				mapName = currentMap.getMapName();
				initStoryGame();
			}
			else
			{
				currentMap = story.getLastMap();
				GameManager.getInstance().displayGUI(new DialogPanel(story.getStory().getStoryEnd(), 1));
			}
		}
		else
		{
			Map map = currentMap;
			while (map == currentMap)
			{
				currentMap = RessourcesManager.getMapList(false).get(random.nextInt(RessourcesManager.getMapList(false).size()));
			}
			mapName = currentMap.getMapName();
			initGame();
		}
		GameManager.getInstance().getCurrentGUI().refreshGUI();
	}

	public void prepareBackgroundMusic()
	{
		if (GameSettings.selectedMusic.equals("Aleatoire"))
		{
			String[] musics = RessourcesManager.getAllMusic();
			backgroundMusic = RessourcesManager.getAudio(musics[RANDOM.nextInt(musics.length - 2) + 2] + ".wav");
			backgroundMusic.loop();
		}
		else if (!GameSettings.selectedMusic.equals("Aucune"))
		{
			backgroundMusic = RessourcesManager.getAudio(GameSettings.selectedMusic + ".wav");
			backgroundMusic.loop();
		}
	}

	public EnumGameMode getGamemode()
	{
		return gamemode;
	}

	public EnumDifficulty getDifficulty()
	{
		return difficulty;
	}

	public Map getCurrentMap()
	{
		return currentMap;
	}

	public void setCurrentMap(Map currentMap)
	{
		this.currentMap = currentMap;
	}

	public int getMaxQuestion()
	{
		return maxQuestion;
	}

	public void setMaxQuestion(int maxQuestion)
	{
		this.maxQuestion = maxQuestion;
	}

	public int getScore()
	{
		return score;
	}

	public void addScore(int score)
	{
		this.score += score * difficulty.getScoreMultiplier();
		if (this.score < 0)
		{
			this.score = 0;
		}
		IngameScreen.updateScoreLabel();
	}

	public int getCurrentQuestion()
	{
		return currentQuestion;
	}

	public void setCurrentQuestion(int currentQuestion)
	{
		this.currentQuestion = currentQuestion;
		IngameScreen.setQuestionLabel(currentQuestion, maxQuestion);
	}

	public void setCurrentTarget(int currentTargetID)
	{
		targetLocation = currentMap.getMapLocations().get(currentTargetID);
		IngameScreen.setQuestLabel(targetLocation.getLocationName());
	}

	public int getTimeLeft()
	{
		return timeLeft;
	}

	public void setTimeLeft(int timeLeft)
	{
		if (timeLeft == -1)
		{
			this.timeLeft -= (int) 1 * difficulty.getTimeMultiplier();
			this.totalTime -= (int) 1 * difficulty.getTimeMultiplier();
		}
		else
		{
			this.timeLeft = timeLeft;
		}
		GameManager.getInstance().getCurrentGUI().refreshGUI();

		if (getTimeLeft() <= 0)
		{
			answerAt(0, 0, null);
		}
	}

	public int getMaxTime()
	{
		return maxTime;
	}

	public void setMaxTime(int maxTime)
	{
		this.maxTime = maxTime;
		GameManager.getInstance().getCurrentGUI().refreshGUI();
	}

	public int getLifeAmount()
	{
		return lifeAmount;
	}

	public void setLifeAmount(int lifeAmount)
	{
		this.lifeAmount = lifeAmount;
		GameManager.getInstance().getCurrentGUI().refreshGUI();
		if (lifeAmount == 0)
		{
			if(gamemode == EnumGameMode.ADVENTURE)
			{
				TimerThread.endTimer();
				SwingUtilities.invokeLater(new Runnable()
				{
					public void run()
					{
						GameManager.getInstance().displayGUI(new DialogPanel(story.getStory().getStoryLost(), 2));
					}
				});
			}
			else
			{
				looseGame();
			}
		}
	}

	public int getTotalTime()
	{
		return totalTime;
	}

	public void setTotalTime(int totalTime)
	{
		this.totalTime = totalTime;
		GameManager.getInstance().getCurrentGUI().refreshGUI();
		if(totalTime > getMaxTotalTime())
		{
			setTotalTime(maxTotalTime);
		}
		if (totalTime <= 0)
		{
			looseGame();
		}
	}

	public int getMaxTotalTime()
	{
		return maxTotalTime;
	}

	public void setMaxTotalTime(int maxTotalTime)
	{
		this.maxTotalTime = maxTotalTime;
		GameManager.getInstance().getCurrentGUI().refreshGUI();
	}
	
	public String getMapName()
	{
		return mapName;
	}
	
	public Story getStory()
	{
		return story;
	}

	public void stopMusic()
	{
		if(backgroundMusic != null)
		{
			backgroundMusic.stopMusic();
		}
	}
	
	public void saveGame()
	{
		try
		{
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./saves/" + format.format(new Date()) + "-" + getGamemode().ordinal() + "-" + getDifficulty().ordinal() +  ".ksv"));
			oos.writeObject(this);
			oos.close();
			JOptionPane.showInternalMessageDialog(GameManager.getInstance().getCurrentGUI().getPanel(), "Sauvegarde termin�e !", "Sauvegarde OK !", JOptionPane.INFORMATION_MESSAGE);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			JOptionPane.showInternalMessageDialog(GameManager.getInstance().getCurrentGUI().getPanel(), "Une erreur est survenue !", "Sauvegarde impossible !", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public static void loadMap(String url)
	{
		try
		{
			File file = new File("./saves/" + url);
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
			Game game = (Game) ois.readObject();
			Map map = RessourcesManager.getMap(game.getMapName());
			
			if(map == null)
			{
				ois.close();
				throw new Exception();
			}
			
			game.setCurrentMap(map);
			ois.close();
			
			GameManager.getInstance().setCurrentGame(game);
			GameManager.getInstance().displayGUI(new IngameScreen());
			
			IngameScreen.setQuestionLabel(0, game.maxQuestion);
			IngameScreen.setQuestLabel(game.targetLocation.getLocationName());
			IngameScreen.updateScoreLabel();
			
			TimerThread timer = new TimerThread();
			timer.start();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
