package com.gp3.model;

public class SlowMessageThread extends Thread
{
	private boolean shouldStop = false;
	
	@Override
	public void run()
	{
		while(!shouldStop)
		{
			GameManager.getInstance().getCurrentGUI().refreshGUI();
			try
			{
				Thread.sleep(50);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void stopThread()
	{
		shouldStop = true;
	}
}
