package com.gp3.model;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.Serializable;

public class Location implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private transient Color editorColor = Color.BLACK;
	private String locationName;
	private Point2D.Float[] area;

	public Location(String name, Point2D.Float[] polygon)
	{
		locationName = name;
		area = polygon;
	}

	public String getLocationName()
	{
		return locationName;
	}

	public void setLocationName(String locationName)
	{
		this.locationName = locationName;
	}

	public Point2D.Float[] getArea()
	{
		return area;
	}

	public boolean isPoint()
	{
		return area.length == 1;
	}

	public Color getEditorColor()
	{
		return editorColor;
	}

	public void setEditorColor(Color editorColor)
	{
		this.editorColor = editorColor;
	}

	@Override
	public String toString()
	{
		return (isPoint() ? "Point : " : "Zone : ") + locationName;
	}
}
