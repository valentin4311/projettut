package com.gp3.model;

import java.io.Serializable;

public class Score implements Serializable, Comparable<Score>
{
	private static final long serialVersionUID = 1L;

	private String playerName;
	private EnumDifficulty difficulty;
	private int score;

	public Score(String name, int score, EnumDifficulty diff)
	{
		playerName = name;
		this.score = score;
		difficulty = diff;
	}

	public int getScore()
	{
		return score;
	}

	public EnumDifficulty getDifficulty()
	{
		return difficulty;
	}

	public String getPlayerName()
	{
		return playerName;
	}

	@Override
	public int compareTo(Score sc)
	{
		return Integer.valueOf(score).compareTo(sc.score) * -1;
	}
}
