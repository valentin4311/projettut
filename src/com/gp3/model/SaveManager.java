package com.gp3.model;

import java.io.File;
import java.util.ArrayList;

public class SaveManager
{	
	public static File getSaveAt(int i)
	{
		File[] saveList = new File("./saves/").listFiles();
		return saveList[saveList.length - i - 1];
	}
	public static ArrayList<String> getAllSaves()
	{
		ArrayList<String> saves = new ArrayList<String>();
		File file = new File("./saves/");
		File[] saveList = file.listFiles();
		
		for(int i = saveList.length - 1;i >= 0;i--)
		{
			if(saveList[i].getName().endsWith(".ksv"))
			{
				saves.add(saveList[i].getName());
			}
		}
		return saves;
	}
}
