package com.gp3.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class ScoresManager implements Serializable
{
	private static final long serialVersionUID = 1L;

	private static ScoresManager instance;

	private ArrayList<Score> adventureList = new ArrayList<Score>();
	private ArrayList<Score> arcadeList = new ArrayList<Score>();
	private ArrayList<Score> survivalList = new ArrayList<Score>();

	public ScoresManager()
	{
		instance = this;
		loadScores();
	}

	public ArrayList<Score> getScoreList(int id)
	{
		return id == 0 ? adventureList : id == 1 ? arcadeList : survivalList;
	}
	public ArrayList<Score> getScoreList(EnumGameMode mode)
	{
		return mode == EnumGameMode.ADVENTURE ? adventureList : mode == EnumGameMode.ARCADE ? arcadeList : survivalList;
	}

	public void saveScores()
	{
		try
		{
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./save.ksv"));
			oos.writeObject(this);
			oos.close();
		}
		catch (Exception e)
		{
			ConsoleLogs.warning("No score file found !");
		}
	}

	public void loadScores()
	{
		try
		{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("./save.ksv"));
			ScoresManager manager = (ScoresManager) ois.readObject();
			if (manager != null)
			{
				instance.adventureList = manager.adventureList;
				instance.arcadeList = manager.arcadeList;
				instance.survivalList = manager.survivalList;
			}
			ois.close();
		}
		catch (Exception e)
		{
			ConsoleLogs.warning("No score file found !");
		}
	}

	public void addScore(String name, int score, EnumDifficulty difficulty, EnumGameMode mode)
	{
		ArrayList<Score> newList = getScoreList(mode);
		newList.add(new Score(name, score, difficulty));
		Collections.sort(newList);
		
		if(newList.size() > 10)
		{
			newList.remove(10);
		}
		saveScores();
	}
	
	public void clearScores(int id)
	{
		getScoreList(id).clear();
		saveScores();
	}

	public static ScoresManager getInstance()
	{
		return instance;
	}
}
