package com.gp3.model;

import java.awt.DisplayMode;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class GameSettings
{
	public static boolean isFullScreen = false;
	public static int currentWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
	public static int currentHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
	public static int musicVolume = 100;
	public static int soundVolume = 100;
	public static String selectedMusic = "Aleatoire";
	public static String gameStyle = "Creme Cafe";

	public static void loadConfigFile()
	{
		try
		{
			File configFile = new File("./config.cfg");
			if (!configFile.exists())
			{
				saveConfigFile();
			}

			BufferedReader reader = new BufferedReader(new FileReader(configFile));
			String currentLine = "";
			while ((currentLine = reader.readLine()) != null)
			{
				try
				{
					if (currentLine.contains("="))
					{
						String[] scheme = currentLine.split("=");

						switch (scheme[0])
						{
							case "fullScreen":
								isFullScreen = Boolean.valueOf(scheme[1]);
								break;
							case "width":
								currentWidth = Integer.valueOf(scheme[1]);
								break;
							case "height":
								currentHeight = Integer.valueOf(scheme[1]);
								break;
							case "musicVolume":
								musicVolume = Integer.valueOf(scheme[1]);
								break;
							case "soundVolume":
								soundVolume = Integer.valueOf(scheme[1]);
								break;
							case "music":
								selectedMusic = scheme[1];
								break;
							case "style":
								gameStyle = scheme[1];
								break;
						}

					}
				}
				catch (Exception e)
				{
					System.err.println("Error while reading line : " + currentLine);
					e.printStackTrace();
				}
			}
			reader.close();
		}
		catch (Exception e)
		{
			System.err.println("Can't load config file");
			saveConfigFile();
		}
	}

	public static void saveConfigFile()
	{
		try
		{
			File configFile = new File("./config.cfg");
			BufferedWriter writer = new BufferedWriter(new FileWriter(configFile));
			writer.write("fullScreen=" + isFullScreen);
			writer.newLine();
			writer.write("width=" + currentWidth);
			writer.newLine();
			writer.write("height=" + currentHeight);
			writer.newLine();
			writer.write("musicVolume=" + musicVolume);
			writer.newLine();
			writer.write("soundVolume=" + soundVolume);
			writer.newLine();
			writer.write("music=" + selectedMusic);
			writer.newLine();
			writer.write("style=" + gameStyle);
			writer.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void resetConfig()
	{
		isFullScreen = false;
		currentWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
		currentHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
		musicVolume = 100;
		soundVolume = 100;
		selectedMusic = "Aleatoire";
		gameStyle = "Creme Cafe";
		saveConfigFile();
	}

	public static ArrayList<Resolution> getAllAvailableResolution()
	{
		ArrayList<Resolution> list = new ArrayList<Resolution>();
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice dev = env.getDefaultScreenDevice();

		for (DisplayMode mode : dev.getDisplayModes())
		{
			if (mode.getWidth() >= 640 && mode.getHeight() >= 420)
			{
				boolean contains = false;
				for (Resolution reso : list)
				{
					if (mode.getWidth() == reso.getResolution().getWidth() && mode.getHeight() == reso.getResolution().getHeight())
					{
						contains = true;
						break;
					}
				}
				if (!contains)
					list.add(new Resolution(mode));
			}
		}
		return list;
	}

	public static DisplayMode getResolutionForSize(int width, int height)
	{
		GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice dev = env.getDefaultScreenDevice();

		for (DisplayMode mode : dev.getDisplayModes())
		{
			if (mode.getWidth() == width && mode.getHeight() == height)
			{
				return mode;
			}
		}
		return dev.getDisplayMode();
	}

	public static int getIndexForSize(int width, int height)
	{
		ArrayList<Resolution> list = getAllAvailableResolution();

		int counter = 0;

		for (Resolution reso : list)
		{
			if (reso.getResolution().getWidth() == width && reso.getResolution().getHeight() == height)
			{
				return counter;
			}
			counter++;
		}
		return 0;
	}
}
