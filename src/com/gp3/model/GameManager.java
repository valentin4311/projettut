package com.gp3.model;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.gp3.view.GUI;
import com.gp3.view.LoadingScreen;
import com.gp3.view.MainFrame;
import com.gp3.view.MainMenu;

public class GameManager
{
	private static GameManager instance;
	private static MainFrame gameFrame;
	private static GUI currentGUI;
	private static Game currentGame;
	private static String currentMenu;

	public GameManager()
	{
		instance = this;
	}

	public void initGame()
	{
		RessourcesManager.addPicture("loadingScreen.png");
		displayGUI(new LoadingScreen());
		RessourcesManager.loadAllRessources();
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					new ScoresManager();
					GameSettings.loadConfigFile();
					setLAFFromName(GameSettings.gameStyle);
					gameFrame = new MainFrame();
					displayGUI(new MainMenu());
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public void displayGUI(GUI gui)
	{
		if (currentGUI != null)
		{
			currentGUI.destroyGUI();
		}
		currentGUI = gui;
		currentGUI.initGUI();

		if (gameFrame != null)
		{
			gameFrame.setContentPane(gui.getPanel());
			gameFrame.paintAll(gameFrame.getGraphics());
		}
	}

	public GUI getCurrentGUI()
	{
		return currentGUI;
	}

	public static GameManager getInstance()
	{
		return instance;
	}

	public void setLAF(String name)
	{
		try
		{
			UIManager.setLookAndFeel(name);
			JFrame.setDefaultLookAndFeelDecorated(!GameSettings.isFullScreen);
			JDialog.setDefaultLookAndFeelDecorated(!GameSettings.isFullScreen);

			if (gameFrame != null)
			{
				gameFrame.dispose();
				gameFrame = new MainFrame();
				MainMenu menu = new MainMenu();
				menu.initGUI();
				menu.handleAction(null, currentMenu);
				gameFrame.setContentPane(menu);
				SwingUtilities.updateComponentTreeUI(gameFrame);
				gameFrame.paintAll(gameFrame.getGraphics());
				if (!GameSettings.isFullScreen)
					gameFrame.pack();

				setMouseCursor(gameFrame);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				setLAF(UIManager.getCrossPlatformLookAndFeelClassName());
			}
			catch (Exception e2)
			{
				e2.printStackTrace();
				System.exit(1);
			}
		}
	}

	public void setLAFFromName(String lafName)
	{
		switch (lafName)
		{
			case "Metal":
				setLAF(UIManager.getCrossPlatformLookAndFeelClassName());
				break;
			case "Nimbus":
				setLAF("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
				break;
			case "Theme OS":
				setLAF(UIManager.getSystemLookAndFeelClassName());
				break;
			case "Business":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceBusinessLookAndFeel");
				break;
			case "Business Acier Bleu":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceBusinessBlueSteelLookAndFeel");
				break;
			case "Business Acier Noir":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceBusinessBlackSteelLookAndFeel");
				break;
			case "Creme":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceCremeLookAndFeel");
				break;
			case "Creme Cafe":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceCremeCoffeeLookAndFeel");
				break;
			case "Sahara":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceSaharaLookAndFeel");
				break;
			case "Modere":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceModerateLookAndFeel");
				break;
			case "Nebula":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceNebulaLookAndFeel");
				break;
			case "Nebula Brick Wall":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceNebulaBrickWallLookAndFeel");
				break;
			case "Automne":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceAutumnLookAndFeel");
				break;
			case "Brume Argente":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceMistSilverLookAndFeel");
				break;
			case "Brume Aquatique":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceMistAquaLookAndFeel");
				break;
			case "Poussiere":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceDustLookAndFeel");
				break;
			case "Poussiere cafe":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceDustCoffeeLookAndFeel");
				break;
			case "Gemini":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceGeminiLookAndFeel");
				break;
			case "Satellite":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceMarinerLookAndFeel");
				break;
			case "Crepuscule":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceTwilightLookAndFeel");
				break;
			case "Magellan":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceMagellanLookAndFeel");
				break;
			case "Graphite":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceGraphiteLookAndFeel");
				break;
			case "Graphite Vitre":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceGraphiteGlassLookAndFeel");
				break;
			case "Graphite Aquatique":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceGraphiteAquaLookAndFeel");
				break;
			case "Corbeau":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceRavenLookAndFeel");
				break;
			case "Challenger Deep":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceChallengerDeepLookAndFeel");
				break;
			case "Poussiere Emeraude":
				setLAF("org.pushingpixels.substance.api.skin.SubstanceEmeraldDuskLookAndFeel");
				break;
			default:
				setLAF(UIManager.getSystemLookAndFeelClassName());
		}
	}

	public void setMouseCursor(JFrame frame)
	{
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image image = RessourcesManager.getPicture("cursor");
		Cursor c = toolkit.createCustomCursor(image, new Point(0, 0), "img");
		frame.setCursor(c);
	}

	public static void toggleFullScreen()
	{
		GameSettings.isFullScreen = !GameSettings.isFullScreen;
		GameManager.getInstance().setLAFFromName(GameSettings.gameStyle);
	}

	public String getCurrentMenu()
	{
		return currentMenu;
	}

	public void setCurrentMenu(String currentMenu)
	{
		GameManager.currentMenu = currentMenu;
	}

	public Game getCurrentGame()
	{
		return currentGame;
	}

	public void setCurrentGame(Game currentGame)
	{
		setMouseCursor(gameFrame);
		if(GameManager.currentGame != null)
		{
			GameManager.currentGame.stopMusic();
		}
		GameManager.currentGame = currentGame;
	}

	public MainFrame getGameFrame()
	{
		return gameFrame;
	}
}
