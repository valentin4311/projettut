package com.gp3.model;

import java.io.Serializable;

public class Story implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String heroName;
	private StoryLine story;
	
	private int currentMap = 0;
	
	public Story(String name, StoryLine line)
	{
		heroName = name;
		this.story = line;
	}
	
	public String getHeroName()
	{
		return heroName;
	}

	public StoryLine getStory()
	{
		return story;
	}

	public Map getCurrentMap()
	{
		return RessourcesManager.getMap(story.getMapOrder()[currentMap].split(":")[0]);
	}

	public String getCurrentEnd()
	{
		return story.getMapOrder()[currentMap].split(":")[1];
	}
	
	public Map getLastMap()
	{
		return RessourcesManager.getMap(story.getMapOrder()[story.getMapOrder().length - 1].split(":")[0]);
	}

	
	public Map nextMap()
	{
		currentMap++;
		if(currentMap < story.getMapOrder().length)
		{
			return getCurrentMap();
		}
		return null;
	}

}
