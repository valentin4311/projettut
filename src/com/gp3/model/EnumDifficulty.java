package com.gp3.model;

public enum EnumDifficulty
{
	EASY(1, 1, "Facile"), MEDIUM(1.25F, 1.5F, "Moyen"), HARD(1.5F, 2.5F, "Difficile"), IMPOSSIBLE(1.75F, 4F, "Impossible");

	private float scoreMultiplier;
	private float timeMultiplier;
	private String name;

	private EnumDifficulty(float scoreMultiplier, float timeMultiplier, String name)
	{
		this.scoreMultiplier = scoreMultiplier;
		this.timeMultiplier = timeMultiplier;
		this.name = name;
	}

	public float getScoreMultiplier()
	{
		return scoreMultiplier;
	}

	public float getTimeMultiplier()
	{
		return timeMultiplier;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
}
