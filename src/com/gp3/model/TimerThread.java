package com.gp3.model;

import com.gp3.view.PauseMenu;

public class TimerThread extends Thread
{
	private static TimerThread timer;
	private boolean shouldEnd = false;

	public TimerThread()
	{
		timer = this;
	}

	@Override
	public void run()
	{
		while (!shouldEnd)
		{
			try
			{
				Thread.sleep(1);
				if (!PauseMenu.isOpen())
				{
					GameManager.getInstance().getCurrentGame().setTimeLeft(-1);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void endTimer()
	{
		if (TimerThread.timer != null)
		{
			TimerThread.timer.shouldEnd = true;
		}
	}
}
