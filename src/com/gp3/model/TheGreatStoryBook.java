package com.gp3.model;

import java.util.Random;

public class TheGreatStoryBook
{
	private static final String[] NAMES = { "Pierre", "Yann", "Bruno", "Nathalie", "Ludovic", "Adrien", "Tawfiq", "S�bastien", "Lahcen", "Esteban", "C�cile", "Tadjine", "Michel", "Jean-Jaques", "Olivier", "Aous", "Christophe", "Adeline", "Herv�", "Adam", "Alex", "Alexandre", "Alexis", "Anthony", "Antoine", "Benjamin", "C�dric", "Charles", "Christopher", "David", "Dylan", "�douard", "Elliot", "�mile", "�tienne", "F�lix", "Gabriel", "Guillaume", "Hugo", "Isaac", "Jacob", "J�r�my", "Jonathan", "Julien", "Justin", "L�o", "Logan", "Lo�c", "Louis", "Lucas", "Malik", "Mathieu", "Mathis", "Maxime", "Micha�l", "Nathan", "Nicolas", "Noah", "Philippe", "Rapha�l", "Samuel", "Simon", "Thomas", "Tommy", "Tristan", "Victor", "Vincent", "Alexia", "Alice", "Alicia", "Am�lie", "Ana�s", "Annabelle", "Arianne", "Audrey", "Aur�lie", "Camille", "Catherine", "Charlotte", "Chlo�", "Clara", "Coralie", "Daphn�e", "Delphine", "Elizabeth", "�lodie", "�milie", "Emma", "Emy", "�ve", "Florence", "Gabrielle", "Jade", "Juliette", "Justine", "Laurence", "Laurie", "L�a", "L�anne", "Ma�lie", "Ma�va", "Maika", "Marianne", "Marilou", "Maude", "Maya", "M�gan", "M�lodie", "Mia", "No�mie", "Oc�ane", "Olivia", "Rosalie", "Rose", "Sarah", "Sofia", "Victoria" };
	private static final StoryLine[] STORIES = 
	{
		new StoryLine(
				new String[]{"Le monde court � sa perte...", "En salle wifi le scientifique QWERTY va le d�truire avec", "sa nouvelle arme... L'UNIX Destroyer !", "%n vous devez le stopper !", "Courrez en salle wifi � l'iut de laval !"}, 
				new String[]{"Vous apercevez le scientifique QWERTY", "\"Ah ah tu ne m'aura pas %n !\"", "Vous d�branchez le Wifi...", "\"Nooooonnnn\" mon plan �tait parfais !\"", "QWERTY s'est �vanoui. Vous avez gagn� !"}, 
				new String[]{"Vous �tes perdu...", "Le Terre explose dans 5 secondes...", "BOOOOOOOOUUUUUUMMMMMM CRACK BOUM", "CRRRRACK BOOOUM", "Game over"},
				new String[]{"France:Laval", "Mayenne:Laval", "Laval:IUT", "D�p. Info:Salle Wifi"}),
		new StoryLine(
				new String[]{"Vous avez un examen aujourd'hui.", "Votre r�veil n'a pas sonn�, vous �tes en retard !", "%n pressez-vous, allez en salle d'examen avant de rater votre semestre !"}, 
				new String[]{"Vous arrivez avec seulement 3 minutes de retard", "Allez vous r�ussir l'examen ?", "C'est une autre histoire...", "Vous avez gagn� ! (Pour le moment)"}, 
				new String[]{"Vous �tes perdu...", "Vous avez trop de retard, l'examen est d�j� termin� !", "Game over"},
				new String[]{"Mayenne:Laval", "Laval:IUT", "IUT de Laval:Salle d'examen"}),
		new StoryLine(
				new String[]{"Vous �tes au McDonald's.", "Vous avez oubli� votre sac � Nantes", "%n, les cours commencent dans 3 heures en TDM2 !", "Il est l'heure de courrir tr�s", "tr�s", "tr�s", "vite !"}, 
				new String[]{"Vous �tes � l'heure !", "Vous avez beaucoup march�, reposez-vous un peu...", "Vous avez gagn� !"}, 
				new String[]{"Vous �tes perdu...", "Vous avez un peu trop de retard !", "Game over"},
				new String[]{"Laval:Gare SNCF", "France:Nantes", "Mayenne:Laval", "D�p. Info:TDM2"}),
		new StoryLine(
				new String[]{"Vous avez trouv� un livre incroyable � la B.U. !", "Vous aimeriez le montrer � vos amis � Strasbourg.", "%n, essayez de ne pas vous perdre sur la route."}, 
				new String[]{"Vos amis aiment beaucoup ce livre.", "Il raconte l'histoire d'une personne", "Qui va � l'autre bout de la France", "Pour montrer un livre � ses amis", "Qui raconte encore et toujours", "Cette m�me histoire. (Storyception)", "Vous avez gagn� !"}, 
				new String[]{"Vous �tes perdu...", "Je vous avais pr�venu !", "Game over"},
				new String[]{"IUT de Laval:Arr�t TUL", "Laval:Gare SNCF", "Mayenne:Evron", "France:Starsbourg"}),
		new StoryLine(
				new String[]{"Vous �tes %n, grand fan du Square de Boston.", "C'est un lieu paisible.", "Actuellement vous �tes � l'IUT", "Rendez-vous au Square !"}, 
				new String[]{"Votre incroyable sentiment de naturophile vous emm�ne au Square", "Vous avez gagn� !"}, 
				new String[]{"Vous �tes perdu...", "Vous ne trouverez plus jamais le square !", "Game over"},
				new String[]{"D�p. Info:Entr�e", "IUT de Laval:Arr�t TUL", "Laval:Square de Boston"})
	};
	
	public static Story tellMeAStory()
	{
		Random random = new Random();
		
		return new Story(NAMES[random.nextInt(NAMES.length)], STORIES[random.nextInt(STORIES.length)]);
	}
}
