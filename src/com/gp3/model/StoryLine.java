package com.gp3.model;

import java.io.Serializable;

public class StoryLine implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String[] storyStart;
	private String[] storyEnd;
	private String[] storyLost;
	private String[] mapOrder;
	
	public StoryLine(String[] storyStart, String[] storyEnd, String[] storyLost, String[] mapOrder)
	{
		this.storyStart = storyStart;
		this.storyEnd = storyEnd;
		this.storyLost = storyLost;
		this.mapOrder = mapOrder;
	}

	public String[] getStoryStart()
	{
		return storyStart;
	}

	public String[] getStoryEnd()
	{
		return storyEnd;
	}

	public String[] getStoryLost()
	{
		return storyLost;
	}

	public String[] getMapOrder()
	{
		return mapOrder;
	}
	
	
}
