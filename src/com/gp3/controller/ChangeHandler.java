package com.gp3.controller;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.gp3.view.GUI;

public class ChangeHandler implements ChangeListener
{
	private final GUI buttonGUI;
	private final JSlider slider;
	private final String buttonID;

	public ChangeHandler(GUI buttonGUI, String buttonID, JSlider slider)
	{
		this.buttonGUI = buttonGUI;
		this.buttonID = buttonID;
		this.slider = slider;
	}

	@Override
	public void stateChanged(ChangeEvent event)
	{
		buttonGUI.handleChangeEvent(event, buttonID, slider);
	}
}
