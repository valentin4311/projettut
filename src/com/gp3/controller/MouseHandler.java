package com.gp3.controller;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.gp3.model.EnumGameMode;
import com.gp3.model.GameManager;
import com.gp3.model.Location;
import com.gp3.model.RessourcesManager;
import com.gp3.view.GUI;
import com.gp3.view.PauseMenu;

public class MouseHandler implements MouseListener, MouseMotionListener
{
	private static Color currentPolygonColor;

	private static Random random = new Random();
	private static ArrayList<Point2D.Float> coordinatesList = new ArrayList<Point2D.Float>();
	private static int mouseX;
	private static int mouseY;
	private static boolean isIn = false;

	public static Color getCurrentPolygonColor()
	{
		return currentPolygonColor;
	}

	public static boolean isIn()
	{
		return isIn;
	}

	public static ArrayList<Point2D.Float> getCoordinatesList()
	{
		return coordinatesList;
	}

	public static int getMouseX()
	{
		return mouseX;
	}

	public static int getMouseY()
	{
		return mouseY;
	}

	public static void changeCursor(boolean isIn)
	{
		MouseHandler.isIn = isIn;
		if (!isIn)
		{
			GameManager.getInstance().setMouseCursor(GameManager.getInstance().getGameFrame());
		}
		else
		{
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Image image = RessourcesManager.getPicture("cursor2");
			Cursor c = toolkit.createCustomCursor(image, new Point(16, 31), "img2");
			GameManager.getInstance().getGameFrame().setCursor(c);
		}
	}

	@Override
	public void mouseMoved(MouseEvent arg0)
	{
		if (PauseMenu.isOpen())
			return;

		mouseX = arg0.getX();
		mouseY = arg0.getY();
		if (GameManager.getInstance().getCurrentGame().getGamemode() == EnumGameMode.EDITOR)
		{
			GameManager.getInstance().getCurrentGUI().refreshGUI();
		}
	}

	@Override
	public void mouseReleased(MouseEvent me)
	{
		if (PauseMenu.isOpen())
			return;

		if (GameManager.getInstance().getCurrentGame().getGamemode() != EnumGameMode.EDITOR)
		{
			GameManager.getInstance().getCurrentGame().answerAt(me.getX(), me.getY(), (JPanel) me.getComponent());
		}
		else
		{
			if (me.getButton() == MouseEvent.BUTTON1)
			{
				if (coordinatesList.isEmpty())
				{
					currentPolygonColor = new Color(random.nextInt(128) + 128, random.nextInt(128) + 128, random.nextInt(128) + 128);
				}
				coordinatesList.add(new Point2D.Float((float) ((double) me.getX() / me.getComponent().getWidth()), (float) ((double) me.getY() / me.getComponent().getHeight())));

				GUI gui = GameManager.getInstance().getCurrentGUI();
				if (gui != null)
				{
					gui.refreshGUI();
				}
			}
			else if (me.getButton() == MouseEvent.BUTTON3)
			{
				if (coordinatesList.size() > 0)
				{
					if (coordinatesList.size() == 2)
					{
						JOptionPane.showInternalMessageDialog(me.getComponent().getParent(), "Une zone doit �tre compos�e de 3 points minimum !", "Zone incorrecte !", JOptionPane.ERROR_MESSAGE);
						return;
					}

					Point2D.Float[] floatArray = new Point2D.Float[coordinatesList.size()];
					coordinatesList.toArray(floatArray);
					boolean isCorrect = true;
					String zoneName = "";
					do
					{
						isCorrect = true;
						zoneName = JOptionPane.showInternalInputDialog(me.getComponent().getParent(), "Entrez le nom " + (coordinatesList.size() == 1 ? " du point" : "de la zone"));

						if (zoneName == null)
						{
							coordinatesList.clear();
							return;
						}

						if (zoneName.length() >= 2 && zoneName.length() <= 16)
						{
							for (Location loc : GameManager.getInstance().getCurrentGame().getCurrentMap().getMapLocations())
							{
								if (loc.getLocationName().equalsIgnoreCase(zoneName))
								{
									isCorrect = false;
									JOptionPane.showInternalMessageDialog(me.getComponent().getParent(), "Une zone comporte le m�me nom !", "Nom incorrecte !", JOptionPane.ERROR_MESSAGE);
									break;
								}
							}
						}
						else
						{
							isCorrect = false;
							JOptionPane.showInternalMessageDialog(me.getComponent().getParent(), "Le nom doit �tre compos� de 2 � 16 caract�res", "Nom incorrecte !", JOptionPane.ERROR_MESSAGE);
						}
					}
					while (!isCorrect);

					Location location = new Location(zoneName, floatArray);
					location.setEditorColor(currentPolygonColor);
					GameManager.getInstance().getCurrentGame().getCurrentMap().getMapLocations().add(location);
					RessourcesManager.getAudio("s-signature.wav").playMusic();
					coordinatesList.clear();

					GUI gui = GameManager.getInstance().getCurrentGUI();
					if (gui != null)
					{
						gui.refreshGUI();
					}
				}
				else
				{
					ArrayList<Location> hittedList = new ArrayList<Location>();
					for (Location location : GameManager.getInstance().getCurrentGame().getCurrentMap().getMapLocations())
					{
						if (location.isPoint())
						{
							Point2D.Float point = location.getArea()[0];
							int distance = (int) Math.sqrt(Math.pow(me.getX() - point.x * me.getComponent().getWidth(), 2) + Math.pow((me.getY() - point.y * me.getComponent().getHeight()), 2));
							if (distance <= 24)
							{
								hittedList.add(location);
							}
						}
						else
						{
							Polygon polygon = new Polygon();
							for (Point2D.Float point : location.getArea())
							{
								polygon.addPoint((int) (point.getX() * me.getComponent().getWidth()), (int) (point.getY() * me.getComponent().getHeight()));
							}
							if (polygon.contains(me.getX(), me.getY()))
							{
								hittedList.add(location);
							}
						}
					}

					if (hittedList.size() > 0)
					{
						Location editLocation = null;

						if (hittedList.size() > 1)
						{
							editLocation = (Location) JOptionPane.showInternalInputDialog(me.getComponent().getParent(), "Il y a plusieurs �l�ments ici.\nVous cherchez :", "Plusieurs choix s'offre � vous...", JOptionPane.PLAIN_MESSAGE, null, hittedList.toArray(), hittedList.get(0));
						}
						else
						{
							editLocation = hittedList.get(0);
						}
						if (editLocation == null)
						{
							return;
						}
						String[] options = { "Renommer", "Supprimer", "Annuler" };
						int result = JOptionPane.showInternalOptionDialog(me.getComponent().getParent(), (editLocation.isPoint() ? "Point : " : "Zone : ") + editLocation.getLocationName() + "\nQue faire ?", "Menu " + editLocation.getLocationName(), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

						if (result == JOptionPane.YES_OPTION)
						{
							boolean isCorrect = true;
							String zoneName = "";
							do
							{
								isCorrect = true;
								zoneName = JOptionPane.showInternalInputDialog(me.getComponent().getParent(), "Entrez le nom " + (coordinatesList.size() == 1 ? " du point" : "de la zone"));

								if (zoneName == null || (zoneName != null && zoneName.equalsIgnoreCase(editLocation.getLocationName())))
								{
									break;
								}

								if (zoneName.length() >= 2 && zoneName.length() <= 16)
								{
									for (Location loc : GameManager.getInstance().getCurrentGame().getCurrentMap().getMapLocations())
									{
										if (loc.getLocationName().equalsIgnoreCase(zoneName))
										{
											isCorrect = false;
											JOptionPane.showInternalMessageDialog(me.getComponent().getParent(), "Une zone comporte le m�me nom !", "Nom incorrecte !", JOptionPane.ERROR_MESSAGE);
											break;
										}
									}
								}
								else
								{
									isCorrect = false;
									JOptionPane.showInternalMessageDialog(me.getComponent().getParent(), "Le nom doit �tre compos� de 2 � 16 caract�res", "Nom incorrecte !", JOptionPane.ERROR_MESSAGE);
								}
							}
							while (!isCorrect);

							if (zoneName != null)
							{
								editLocation.setLocationName(zoneName);
								RessourcesManager.getAudio("s-signature.wav").playMusic();
							}
						}
						else if (result == JOptionPane.NO_OPTION)
						{
							GameManager.getInstance().getCurrentGame().getCurrentMap().getMapLocations().remove(editLocation);
						}
					}
				}
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0)
	{
		mouseMoved(arg0);
	}

	@Override
	public void mouseEntered(MouseEvent arg0)
	{
		if (PauseMenu.isOpen())
			return;

		changeCursor(true);
	}

	@Override
	public void mouseExited(MouseEvent arg0)
	{
		if (PauseMenu.isOpen())
			return;

		changeCursor(false);
	}

	@Override
	public void mouseClicked(MouseEvent me)
	{}

	@Override
	public void mousePressed(MouseEvent arg0)
	{}

}
