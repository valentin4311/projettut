package com.gp3.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.gp3.view.GUI;

public class EventHandler implements ActionListener
{
	private final GUI buttonGUI;
	private final String buttonID;

	public EventHandler(GUI buttonGUI, String buttonID)
	{
		this.buttonGUI = buttonGUI;
		this.buttonID = buttonID;
	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		buttonGUI.handleAction(event, buttonID);
	}
}
